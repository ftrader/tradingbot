# Algorithmic trading bot
This project is about eventually creating a algorithmic trading bot for stocks.


## Backend API
The goal of the backend side is creating API for retrieving and storing data, backtesting strategies, trigger new data downloads and eventually running a completed bot.

It is built on:
.NET Core 2.1
EntityFramework
Swagger


## Frontend
The goal of the frontend is mainly to make an interface for validating backtested strategies. Given a strategy for creating buy/sell signals, it will draw up a graph of all points in time when the strategy bought/sold stocks. 

It is built on:
Axios
React timeseries chart

## Strategies

** Existing strategies **

**Simple Moving Average**
Calculates the average of a set number of days. Buy if close is lower than the average. Sell otherwise.

**Moving Average**
Calculates the average of a set number of days for a lower and upper bound. Buy if the lower bound is above the higher bound. Sell otherwise.

**Exponential Moving Average** 
Same as **Moving Average** but with instead of equal weights for each close an exponential part is used which weighs the most recent close prices higher.

**Creating a new strategy**
To create a new strategy, you only need to use the strategy pattern already implemented. Create a class called XyzStrategy which should implement the interface TradingStrategy. The system will find all implementations of any TradingStrategy and expose them to the frontend using Reflection.

## Deployment:
Currently I have used Google Cloud Platform for hosting. Backend is hosted in the Flexible App Engine and the frontend is aimed to be dockerized to run in an container at Google Cloud Platform.

## Authors

Fredrik Larsson
