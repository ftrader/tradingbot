﻿namespace Api.Utils
{
    public static class FormatUtils
    {
        public static decimal RoundToTwoDecimals(this decimal value)
        {
            return Math.Round(value, 2);
        }
    }
}