﻿using Api.EntityFramework.Entities;

namespace Api.Utils
{
    public static class IndicatorUtils
    {
        public static IEnumerable<Candle> FilterByDate(this IEnumerable<Candle> candles, DateTime? from, DateTime? to)
        {
            if (from != null)
            {
                candles = candles.Where(x => x.Date >= from);
            }

            if (to != null)
            {
                candles = candles.Where(x => x.Date <= to);
            }

            return candles;
        }
        
        public static decimal MovingAverage(IEnumerable<Candle> candles, int days)
        {
            var lastCandle = candles.Last();
            return candles.Where(x => x.Date >= lastCandle.Date.AddDays(-days)).Average(x => x.Close);
        }

        public static decimal ExponentialMovingAverage(IEnumerable<Candle> candles, decimal previousValue, decimal alpha)
        {
            if (previousValue == 0)
            {
                return candles.Last().Close;
            }

            return (1 - alpha) * previousValue + alpha * candles.Last().Close;
        }

        public static decimal RSI(IEnumerable<Candle> candles, int periods)
        {
            var availableCandles = candles.TakeLast(periods).ToList();
            if (availableCandles.Count() < periods)
            {
                return 50m;
            }
        
            var up = availableCandles.Where(x => x.AdjChange >= 0);
            if (!up.Any())
            {
                return 0m;
            }
            var upAvg = up.Average(x => x.AdjChange);
            
            var down = availableCandles.Where(x => x.AdjChange < 0);
            if (!down.Any())
            {
                return 100m;
            }
            var downAvg = Math.Abs(down.Average(x => x.AdjChange));
        
            var rsi = 100 - 100 / (1 + upAvg / downAvg);
            return rsi;
        }
    }
}