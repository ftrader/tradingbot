﻿using Api.Queries;

namespace Api.Utils.Cache
{
    public static class CacheKey
    {
        public static readonly string GetSymbolsQuery = typeof(GetAllSymbolsQuery).Name;

        public static string GetCandlesQuery(string symbol)
        {
            return $"{typeof(GetCandlesQuery).Name}-{symbol}";
        }
    }
}