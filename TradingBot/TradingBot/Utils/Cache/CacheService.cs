﻿using LazyCache;
using Api.EntityFramework.Context;

namespace Api.Utils.Cache
{
    public interface IClearCacheCommand
    {
        void Execute();
    }
    
    public class ClearCacheCommand : IClearCacheCommand
    {
        private readonly IAppCache _cache;
        private readonly SqlDbContext _dbContext;

        public ClearCacheCommand(IAppCache cache, SqlDbContext dbContext)
        {
            _cache = cache;
            _dbContext = dbContext;
        }

        public void Execute()
        {
            _cache.Remove(CacheKey.GetSymbolsQuery);
            var symbols = _dbContext.Symbols.Select(x => x.Symbol);
            foreach (var symbol in symbols)
            {
                _cache.Remove(CacheKey.GetCandlesQuery(symbol));
            }
        }
    }
}
