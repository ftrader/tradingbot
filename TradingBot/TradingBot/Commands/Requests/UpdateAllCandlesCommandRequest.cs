﻿using MediatR;

namespace Api.Controllers.Commands
{
    public class UpdateAllCandlesCommandRequest : IRequest<Unit>
    {
    }
}