﻿using MediatR;

namespace Api.Controllers.Commands
{
    public class AddSymbolsCommandRequest : IRequest<Unit>
    {
        public IEnumerable<string> Symbols { get; }

        public AddSymbolsCommandRequest(IEnumerable<string> symbols)
        {
            Symbols = symbols;
        }
    }
}
