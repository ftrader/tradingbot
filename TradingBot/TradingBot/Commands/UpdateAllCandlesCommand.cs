﻿using LazyCache;
using MediatR;
using Api.Domain.Yahoo;
using Api.EntityFramework.Context;
using Api.Utils.Cache;

namespace Api.Controllers.Commands
{
    public class UpdateAllCandlesCommand : IRequestHandler<UpdateAllCandlesCommandRequest>
    {
        private readonly IDownloadTicketService _yahooService;
        private readonly IAppCache _cache;
        private readonly SqlDbContext _dbContext;

        public UpdateAllCandlesCommand(IDownloadTicketService yahooService, IAppCache cache, SqlDbContext dbContext)
        {
            _yahooService = yahooService;
            _cache = cache;
            _dbContext = dbContext;
        }

        public async Task<Unit> Handle(UpdateAllCandlesCommandRequest request, CancellationToken cancellationToken)
        {
            await _yahooService.UpdateStocks();
            var symbolsUpdated = _dbContext.Symbols.Select(x => x.Symbol);
            foreach (var symbol in symbolsUpdated)
            {
                _cache.Remove(CacheKey.GetCandlesQuery(symbol));
            }
            
            _cache.Remove(CacheKey.GetSymbolsQuery);
            return Unit.Value;
        }
    }
}
