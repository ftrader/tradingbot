﻿using LazyCache;
using MediatR;
using Api.Domain.Yahoo;
using Api.Utils.Cache;

namespace Api.Controllers.Commands
{
    public class AddSymbolsCommand : IRequestHandler<AddSymbolsCommandRequest>
    {
        private readonly IDownloadTicketService _yahooService;
        private readonly IAppCache _cache;

        public AddSymbolsCommand(IDownloadTicketService yahooService, IAppCache cache)
        {
            _yahooService = yahooService;
            _cache = cache;
        }

        public async Task<Unit> Handle(AddSymbolsCommandRequest request, CancellationToken cancellationToken)
        {
            foreach (var symbol in request.Symbols)
            {
                await _yahooService.AddStock(symbol);
            }
            _cache.Remove(CacheKey.GetSymbolsQuery);
            return Unit.Value;
        }
    }
}
