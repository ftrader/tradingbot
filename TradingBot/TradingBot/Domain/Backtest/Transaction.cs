﻿using Api.EntityFramework.Entities;

namespace Api.Domain.Backtest
{
    public class Position
    {
        public Candle Entry { get; set; }
        public Candle Exit { get; set; }
        public int Quantity { get; set; }
        
        public decimal EntryPrice()
            => Entry.Close;

        public decimal ExitPrice()
            => Exit.Close;
    }
}