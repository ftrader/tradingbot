﻿namespace Api.Domain.Backtest
{
    public class Day
    {
        public Day(DateTime date, List<string> symbols)
        {
            Date = date;
            Symbols = symbols;
        }

        public DateTime Date { get; set; }
        public List<string> Symbols { get; set; }
    }
}
