﻿namespace Api.Domain.Backtest
{
    public class BacktestStatistics
    {
        public decimal MaximumDrawdown { get; set; }
        public decimal WinRate { get; set; }
        public decimal TotalTrades { get; set; }
        public decimal BuyAndHoldChange { get; set; }
        public decimal Balance { get; set; }
        public decimal PriceChange { get; set; }
        public decimal PercentageChange { get; set; }
        public object TotalTransactionFeeCost { get; set; }
        public decimal ProfitFactor { get; internal set; }
    }
}