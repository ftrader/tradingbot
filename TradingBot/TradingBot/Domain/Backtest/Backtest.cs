﻿using Api.EntityFramework.Entities;
using Api.ReactModels.Graph;
using Api.Strategies.Investment;
using Api.Strategies.Signal;

namespace Api.Domain.Backtest
{
    public class Backtest
    {
        private Dictionary<string, List<Candle>> _stocks = new();
        private readonly Dictionary<string, TradingStrategyBase> _strategies = new();
        
        private bool UseSameTradingStrategyInstance { get; set; }
        private TradingStrategyBase _strategy;
        private IEnumerable<string> Symbols { get; set; }
        private TransactionLog TransactionLog { get; set; }
        private InvestmentStrategy InvestStrategy { get; set; }
        
        public Backtest() { }

        public void AddStocksAndStrategies(Dictionary<string, List<Candle>> stocks, TradingStrategyBase strategy, InvestmentStrategy investmentStrategy)
        {
            _stocks = stocks;
            Symbols = stocks.Select(kvp => kvp.Key);
            
            InvestStrategy = investmentStrategy;

            // Setup strategy
            UseSameTradingStrategyInstance = !strategy.UseUniqueInstance();
            if (UseSameTradingStrategyInstance)
            {
                UseSameTradingStrategyInstance = true;
                _strategy = strategy;
            }
            else
            {
                foreach (var symbol in Symbols)
                {
                    _strategies.Add(symbol, strategy.CopyInstance());
                }
            }
            
            TransactionLog = new TransactionLog()
            {
                AverageBuyAndHoldPercentageIncrease = AverageBuyAndHoldPercentageIncrease()
            };
        }
        
        public void Configure(decimal startBalance = 10000, decimal transactionFee = 0.0025m)
        {
            TransactionLog.Configure(startBalance, transactionFee);
        }

        private decimal AverageBuyAndHoldPercentageIncrease()
        {
            return _stocks.Average(stock => (stock.Value.Last().Close / stock.Value.First().Close) * 100 - 100);
        }

        private TradingStrategyBase GetStrategy(string symbol)
        {
            if (UseSameTradingStrategyInstance)
            {
                return _strategy;
            }

            return _strategies[symbol];
        }

        public void Execute()
        {
            var indexDictionary = GetDictionaryWithZeros();

            var currentDate = _stocks.Values.Min(x => x.First().Date);
            var lastDate = _stocks.Values.Max(x => x.Last().Date);

            while (currentDate <= lastDate)
            {
                foreach (var symbol in Symbols)
                {
                    var symbolDayIndex = indexDictionary[symbol];

                    if (symbolDayIndex >= _stocks[symbol].Count - 1)
                    {
                        continue;
                    }

                    var todaysCandle = _stocks[symbol][symbolDayIndex];
                    if (todaysCandle.Date != currentDate.Date)
                    {
                        continue; // This stock didn't have tick for this day
                    }

                    ExecuteTick(symbol, todaysCandle);

                    // Increment
                    symbolDayIndex++;
                    indexDictionary[symbol] = symbolDayIndex;
                }

                currentDate = currentDate.AddDays(1);
                TransactionLog.CalculateForEndOfDay(currentDate);
            }

            SellAllOpenPositions();
        }

        public TransactionLog GetLog()
        {
            return TransactionLog;
        }

        private Dictionary<string, int> GetDictionaryWithZeros()
        {
            var indexDictionary = new Dictionary<string, int>();
            foreach (var symbol in Symbols)
            {
                indexDictionary[symbol] = 0;
            }
            return indexDictionary;
        }

        private void ExecuteTick(string symbol, Candle todaysCandle)
        {
            var strategy = GetStrategy(symbol);
            strategy.AddCandle(todaysCandle);

            TransactionLog.CalculateForStock(todaysCandle);

            if (strategy.BuySignal())
            {
                var quantity = InvestStrategy.GetQuantityToBuy(TransactionLog, todaysCandle);
                TransactionLog.Buy(todaysCandle, quantity);
            }
            else if (TransactionLog.StockHasOpenPosition(symbol) && strategy.SellSignal())
            {
                var quantity = InvestStrategy.GetQuantityToSell(TransactionLog, todaysCandle);
                if (quantity > 0)
                {
                    TransactionLog.Sell(todaysCandle, quantity);
                }
            }
        }

        private void SellAllOpenPositions()
        {
            while (TransactionLog.CurrentPositions.Count > 0)
            {
                var position = TransactionLog.CurrentPositions[0];
                var symbol = position.Entry.Symbol;
                var lastCandle = _stocks[symbol].Last();
                TransactionLog.Sell(lastCandle, position.Quantity);
            }

            TransactionLog.ReCalculateLastDay();
        }

        public List<GraphData> GetSignalChartForSymbol(string symbol) 
            => GetStrategy(symbol).GetSignalGraphs();

        public GraphData GetRSIChartForSymbol(string symbol) 
            => GetStrategy(symbol).GetRSIGraph();
    }
}
