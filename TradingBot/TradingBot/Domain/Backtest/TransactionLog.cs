﻿using Api.EntityFramework.Entities;
using Api.ReactModels.Graph;
using Api.Utils;

namespace Api.Domain.Backtest
{
    public class TransactionLog
    {
        private decimal _startBalance;
        private readonly List<Tick> _equityCurve = new List<Tick>();
        private decimal _transactionFee;

        public decimal Balance { get; set; }
        public decimal TotalTransactionFeeCost { get; set; }
        public int TotalTrades { get; set; }
        public List<Position> ClosedPositions { get; private set; }
        public List<Position> CurrentPositions { get; }
        public decimal AverageBuyAndHoldPercentageIncrease { get; set; }
        private IDictionary<string, decimal> LastPricePerStock { get; }

        public TransactionLog(decimal startBalance = 100000, decimal transactionFee = 0.0025m)
        {
            Configure(startBalance, transactionFee);
            
            ClosedPositions = new List<Position>();
            CurrentPositions = new List<Position>();
            LastPricePerStock = new Dictionary<string, decimal>();
        }

        public void Configure(decimal startBalance, decimal transactionFee)
        {
            _startBalance = startBalance;
            Balance = startBalance;
            _transactionFee = transactionFee;
        }
        
        public void Buy(Candle candle, int quantity)
        {
            var maxBuyQuantity = MaxBuyQuantity(candle, quantity);
            if (maxBuyQuantity == 0)
            {
                return;
            }

            var openPosition = new Position
            {
                Entry = candle,
                Quantity = maxBuyQuantity
            };
            CurrentPositions.Add(openPosition);
            
            Balance -= StockValue(candle, maxBuyQuantity);
            var fee = Fee(candle, maxBuyQuantity);
            Balance -= fee;
            TotalTransactionFeeCost += fee;
            TotalTrades++;
        }

        private int MaxBuyQuantity(Candle candle, int quantity)
        {
            var costOfBuyingOneStock = candle.Close * (1 + _transactionFee);
            var maxAvailableQuantityToBuy = (int) Math.Floor(Balance / costOfBuyingOneStock);
            return Math.Min(maxAvailableQuantityToBuy, quantity);
        }

        private int MaxSellQuantity(Candle candle, int quantity)
        {
            var currentStockQuantity = CurrentQuantityOfSymbol(candle.Symbol);
            return Math.Min(currentStockQuantity, quantity);   
        }

        public void Sell(Candle candle, int quantity)
        {
            var sellQuantity = MaxSellQuantity(candle, quantity);
            while (sellQuantity > 0)
            {
                var position = CurrentPositions.First(x => x.Entry.Symbol == candle.Symbol);

                var sellQuantityForPosition = Math.Min(sellQuantity, position.Quantity);
                if (position.Quantity > sellQuantity)
                {
                    
                    var sellPosition = new Position
                    {
                        Entry = position.Entry,
                        Exit = candle,
                        Quantity = sellQuantityForPosition
                    };
                    
                    // create sell position
                    ClosedPositions.Add(sellPosition);
                    position.Quantity -= sellQuantityForPosition;
                }
                else
                {
                    position.Exit = candle;
                 
                    // move position   
                    ClosedPositions.Add(position);
                    CurrentPositions.Remove(position);
                }
                
                Balance += StockValue(candle, sellQuantityForPosition);
                var fee = Fee(candle, sellQuantityForPosition);
                Balance -= fee;
                TotalTransactionFeeCost += fee;
                sellQuantity -= sellQuantityForPosition;
            }
            
            TotalTrades++;
        }
        
        public void CalculateForStock(Candle stock)
        {
            LastPricePerStock[stock.Symbol] = stock.Close;
        }

        public void CalculateForEndOfDay(DateTime date)
        {
            var currentAmount = CurrentTotalAmount();
            _equityCurve.Add(new Tick(date, currentAmount));
        }

        public void ReCalculateLastDay()
        {
            var currentAmount = CurrentTotalAmount();
            _equityCurve.Last().Close = currentAmount;
        }
        
        private int CurrentQuantityOfSymbol(string symbol)
        {
            return CurrentPositions
                .Where(x => x.Entry.Symbol == symbol)
                .Sum(x => x.Quantity);
        }
        
        public decimal CurrentInvestmentInStock(string symbol)
        {
            return CurrentPositions
                .Where(x => x.Entry.Symbol == symbol)
                .Sum(x => x.Quantity * x.Entry.Close);
        }
        
        public decimal CurrentTotalAmount()
        {
            var amountInOpenPositions = CurrentPositions.Sum(x => x.Quantity * LastPricePerStock[x.Entry.Symbol]);
            return Balance + amountInOpenPositions - amountInOpenPositions * _transactionFee;
        }
        
        private decimal StockValue(Candle candle, int quantity)
        {
            return candle.Close * quantity;
        }

        private decimal Fee(Candle candle, int quantity)
        {
            return candle.Close * quantity * _transactionFee;
        }
        
        public bool StockHasOpenPosition(string symbol)
        {
            return CurrentPositions.Any(x => x.Entry.Symbol == symbol);
        }
        
        public int QuantityFor(string symbol)
        {
            return CurrentPositions.Where(x => x.Entry.Symbol == symbol).Sum(x => x.Quantity);
        }

        public IEnumerable<Tick> GetBuyTicks(string symbol)
        {
            return ClosedPositions
                .Where(x => x.Entry.Symbol == symbol)
                .Select(x => new Tick(x.Entry));
        }
        
        public IEnumerable<Tick> GetSellTicks(string symbol)
        {
            return ClosedPositions
                .Where(x => x.Entry.Symbol == symbol)
                .Select(x => new Tick(x.Exit));
        }

        public decimal BalancePercentageChange()
        {
            return ((Balance / _startBalance) * 100) - 100;
        }

        public List<Trade> GetTradeStats()
        {
            var t = ClosedPositions
                .Select(position => 
                    new Trade
                    {
                        Change = (position.ExitPrice() / position.EntryPrice()) - 1m - _transactionFee,
                        BalanceChange = (position.ExitPrice() - position.EntryPrice()) * position.Quantity, 
                        Quantity = position.Quantity
                    })
                .OrderByDescending(x => x.Change)
                .ToList();
            return t;
        }
        
        public decimal CalculateWinRate()
        {
            var wins = 0;
            var losses = 0;
            foreach (var position in ClosedPositions)
            {
                var totalFeeCost = (position.EntryPrice() + position.ExitPrice()) * _transactionFee;
                
                var priceIncrease = position.ExitPrice() - position.EntryPrice();
                if (priceIncrease > totalFeeCost)
                {
                    wins++;
                }
                else
                {
                    losses++;
                } 
            }

            if (losses + wins == 0)
            {
                return 0;
            }
            
            return (100 * wins) / (decimal) (losses + wins);
        }

        public IEnumerable<Tick> GetEquityCurve() => _equityCurve;

        private decimal CalculateMaximumDrawdown()
        {
            var maxDrawdown = 0m;
            
            foreach (var tick in _equityCurve.Take(_equityCurve.Count - 1))
            {
                var lowestValueAfterTick = _equityCurve.Where(x => x.Date > tick.Date)
                    .Min(x => x.Close);
                
                var difference = (lowestValueAfterTick / tick.Close) - 1;
                if (difference < maxDrawdown)
                {
                    maxDrawdown = difference;
                }
            }
            
            return Math.Abs(maxDrawdown) * 100;
        }
        
        public BacktestStatistics GetStatistics()
        {
            var statistics = new BacktestStatistics
            {
                MaximumDrawdown = CalculateMaximumDrawdown().RoundToTwoDecimals(),
                WinRate = CalculateWinRate().RoundToTwoDecimals(),
                TotalTrades = TotalTrades,
                PercentageChange = BalancePercentageChange().RoundToTwoDecimals(),
                BuyAndHoldChange = AverageBuyAndHoldPercentageIncrease.RoundToTwoDecimals(),
                Balance = Balance.RoundToTwoDecimals(),
                PriceChange = (Balance - _startBalance).RoundToTwoDecimals(),
                TotalTransactionFeeCost = TotalTransactionFeeCost.RoundToTwoDecimals(),
                ProfitFactor = 2.3m
            };

            return statistics;
        }
    }
}
