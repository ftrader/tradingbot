﻿namespace Api.Domain.Yahoo
{
    public interface IDownloadTicketService
    {
        Task AddStock(string symbol);
        Task UpdateStocks();
    }
}