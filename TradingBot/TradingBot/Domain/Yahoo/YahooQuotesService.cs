﻿using Microsoft.EntityFrameworkCore;
using Api.EntityFramework.Entities;
using Api.EntityFramework.Context;
using YahooQuotesApi;

namespace Api.Domain.Yahoo
{
    public class YahooQuotesService : IDownloadTicketService
    {
        private readonly SqlDbContext _dbContext;

        public YahooQuotesService(SqlDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddStock(string symbol)
        {
            if (SymbolExists(symbol))
            {
                // stock exists
                return;
            }

            var security = await YahooClient.GetSecurityAsync(symbol);
            if (security is null)
            {
                return;
            }

            var entity = new SymbolEntity
            {
                Name = security.LongName,
                ShortName = security.ShortName,
                Symbol = symbol,
                LastFetched = Yesterday(),
                HasData = false
            };

            await _dbContext.Symbols.AddAsync(entity);
            await _dbContext.SaveChangesAsync();

            await DownloadCandles(entity);
        }

        public async Task UpdateStocks()
        {
            var symbols = await _dbContext.Symbols
                .Where(x => x.HasData)
                .ToListAsync();
            foreach (var symbol in symbols)
            {
                await DownloadCandles(symbol);
            }
        }

        private async Task DownloadCandles(SymbolEntity symbolEntity)
        {
            var newCandles = await GetCandlesFromDate(symbolEntity.Symbol);

            if (newCandles.Any())
            {
                await SaveCandles(newCandles, symbolEntity);
            }
        }

        private async Task SaveCandles(IEnumerable<Candle> candles, SymbolEntity symbol)
        {
            await _dbContext.Candles.AddRangeAsync(candles);
            symbol.HasData = true;
            symbol.LastFetched = Yesterday();
            await _dbContext.SaveChangesAsync();
        }

        private async Task<List<Candle>> GetCandlesFromDate(string symbol)
        {
            var latestCandle = await GetLastCandleForSymbol(symbol);
            var fromDate = latestCandle?.Date.AddDays(1) ?? new DateTime(1990, 1, 1);
            var candles = await YahooClient.GetHistoricalAsync(symbol, fromDate);

            if (candles.Any())
            {
                return await ConvertToEntities(candles, symbol);
            }

            return new List<Candle>();
        }

        private async Task<List<Candle>> ConvertToEntities(IReadOnlyCollection<PriceTick> candles, string symbol)
        {
            candles = candles.Where(x => x.AdjustedClose != 0).ToList();

            var entities = new List<Candle>();
            var lastAdjClose = (decimal) candles.First().AdjustedClose;

            // Retrieve last candle from database for correct percentage change calculation for new candles
            var lastCandle = await GetLastCandleForSymbol(symbol);
            if (lastCandle is not null)
            {
                lastAdjClose = lastCandle.AdjClose;
            }

            foreach (var candle in candles)
            {
                var adjChange = ((decimal) candle.AdjustedClose / lastAdjClose - 1) * 100;

                entities.Add(new Candle(candle, adjChange, symbol));
                lastAdjClose = (decimal) candle.AdjustedClose;
            }

            return entities;
        }

        private async Task<Candle?> GetLastCandleForSymbol(string symbol)
        {
            return await _dbContext.Candles
                .Where(x => x.Symbol == symbol)
                .OrderBy(x => x.Date)
                .LastOrDefaultAsync();
        }

        private bool SymbolExists(string symbol)
        {
            using var db = new SqlDbContext();
            return db.Symbols.Any(x => x.Symbol == symbol);
        }

        private static DateTime Yesterday()
        {
            return new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day).AddDays(-1);
        }

        private static DateTime TodayAfter1730()
        {
            return new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 17, 30, 0);
        }
    }
}
