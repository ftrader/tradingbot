﻿using NodaTime;
using YahooQuotesApi;

namespace Api.Domain.Yahoo;

public class SecurityNotAvailableException : Exception
{
    public SecurityNotAvailableException(string message) : base(message)
    {
    }
}

public static class YahooClient
{

    public static async Task<Security?> GetSecurityAsync(string symbol)
    {
        YahooQuotes yahooQuotes = new YahooQuotesBuilder().Build();
        return await yahooQuotes.GetAsync(symbol);
    }

    public static async Task<PriceTick[]> GetHistoricalAsync(string symbols, DateTime from)
    {
        if (from.DayOfWeek == DayOfWeek.Sunday)
        {
            from = from.AddDays(-2);
        }
        else if (from.DayOfWeek == DayOfWeek.Saturday)
        {
            from = from.AddDays(-1);
        }

        var fromInstant = Instant.FromUtc(from.Year, from.Month, from.Day, 0, 0);
        YahooQuotes yahooQuotes = new YahooQuotesBuilder()
            .WithHistoryStartDate(fromInstant)
            .Build();

        var security = await yahooQuotes.GetAsync(symbols, HistoryFlags.PriceHistory);
        if (security is null)
        {
            throw new SecurityNotAvailableException("Could not find any security");
        }
        try
        {

            DateTimeZone tz = DateTimeZoneProviders.Tzdb.GetSystemDefault();
            Instant now = SystemClock.Instance.GetCurrentInstant();

            return security.PriceHistory.Value
                .OrderBy(candle => candle.Date)
                .Where(candle => candle.Date.ToDateTimeUnspecified() >= from)
                .ToArray();
        }
        catch
        {
        }

        return Array.Empty<PriceTick>();
    }
}
