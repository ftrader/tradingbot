﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Api.Queries;
using Api.ReactModels.Graph;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CandleController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CandleController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<List<GraphData>> Get([FromBody] GetGraphModel model)
        {
            var charts = new List<GraphData>();
            try
            {
                foreach (var symbol in model.Symbols)
                {
                    var candles = await _mediator.Send(new GetCandlesQueryRequest(symbol, model.From, model.To));
                    var ticks = candles.Select(x => new Tick(x)).ToList();
                    charts.Add(new GraphData(symbol, ticks));
                }

            }
            catch (Exception e)
            {
                var t = e;
            }
            return charts;
        }
    }
}
