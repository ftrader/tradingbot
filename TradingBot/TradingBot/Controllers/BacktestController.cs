﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Api.Queries;
using Api.Domain.Backtest;
using Api.EntityFramework.Entities;
using Api.ReactModels.Graph;
using Api.Strategies.Investment;
using Api.Strategies.Signal;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BacktestController : ControllerBase
    {
        private readonly IMediator _mediator;

        public BacktestController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("run")]
        public async Task<BacktestResultSummary> Get([FromBody] BacktestRequest request)
        {
            var strategy = GetStrategyInstance(request.StrategyName);

            var list = new Dictionary<string, List<Candle>>();
            foreach (var symbol in request.Symbols)
            {
                list[symbol] = await _mediator.Send(new GetCandlesQueryRequest(symbol, request.From, request.To));
            }
            
            var backtest = new Backtest();
            backtest.AddStocksAndStrategies(list, strategy, new PercentageInvestmentStrategy(50));
            backtest.Configure();
            backtest.Execute();
            
            var log = backtest.GetLog();

            var summary = new BacktestResultSummary
            {
                TradePercentages = log.GetTradeStats(),
                SymbolBacktestResults = request.Symbols
                    .Select(symbol => new SymbolBacktestResult
                    {
                        Symbol = symbol,
                        BuyEvents = new GraphData("Buy events", log.GetBuyTicks(symbol)),
                        SellEvents = new GraphData("Sell events", log.GetSellTicks(symbol)),
                        SignalCharts = backtest.GetSignalChartForSymbol(symbol),
                        RSIChart = backtest.GetRSIChartForSymbol(symbol)
                    }).ToList(),
                BacktestStatistics = log.GetStatistics(),
                EquityCurve = new GraphData("Equity Curve", log.GetEquityCurve())
            };

            return summary;
        }

        private TradingStrategyBase GetStrategyInstance(string strategyName)
        {
            var strategyType = Type.GetType(strategyName)!;
            var instance = (TradingStrategyBase) Activator.CreateInstance(strategyType)!;
            return instance;
        }
    }
}
