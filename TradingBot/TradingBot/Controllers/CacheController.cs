﻿using Microsoft.AspNetCore.Mvc;
using Api.Utils.Cache;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CacheController : ControllerBase
    {
        private readonly IClearCacheCommand _clearCacheCommand;

        public CacheController(IClearCacheCommand clearCacheCommand)
        {
            _clearCacheCommand = clearCacheCommand;
        }

        [HttpPost]
        public ActionResult ClearCache()
        {
            _clearCacheCommand.Execute();
            return Ok();
        }
    }
}