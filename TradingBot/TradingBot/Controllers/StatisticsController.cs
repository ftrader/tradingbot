﻿using Api.EntityFramework.Context;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatisticsController : ControllerBase
    {
        private IMediator _mediator;
        private readonly SqlDbContext _dbContext;

        public StatisticsController(IMediator mediator, SqlDbContext dbContext)
        {
            _mediator = mediator;
            _dbContext = dbContext;
        }

        [HttpGet("test")]
        public string GetProbabilities()
        {
            var dict = new Dictionary<int, int>();
            for (int i = 1; i < 15; i++)
            {
                dict[i] = 0;
            }

            var symbols = _dbContext.Symbols.Where(x => x.HasData).Take(50).ToArray();
            foreach(var symbol in symbols)
            {
                var candles = _dbContext.Candles
                   .Where(x => x.Symbol == symbol.Symbol)
                   .OrderBy(x => x.Date)
                   .ToArray();

                var red = 0;
                for (int i = 0; i < candles.Length; i++)
                {
                    var candle = candles[i];

                    if (candle.AdjChange >= 0)
                    {
                        if (red > 0)
                        {
                            dict[red] = dict[red] + 1;
                            red = 0;
                        }
                        continue;
                    }
                    else
                    {
                        red++;
                    }
                }
            }

            var total = dict.Sum(x => x.Value);
            var t = new List<Test>();
            for (int i = dict.Min(x => x.Key); i < dict.Count(); i++)
            {
                var val = dict[i];
                if (val == 0) break;

                var totalLeft = dict.Where(x => x.Key > i).Sum(x => x.Value);
                var probability = totalLeft == 0 ? 1m : (decimal)val / (val + totalLeft);
                t.Add(new Test { PercentageOfCasesEndedHere = (decimal)val /total , ProbabilityOfGreenNextDay = probability });
            }

            return "";
        } 
    }

    // ProbabilityOfGreenNextDay
    //[0]	0.5362817403355937237044003366	decimal
    //[1]	0.5406948817847040614826279554	decimal
    //[2]	0.5575180106902161282825935394	decimal
    //[3]	0.5555409663865546218487394958	decimal
    //[4]	0.5586410635155096011816838996	decimal
    //[5]	0.5642570281124497991967871486	decimal
    //[6]	0.5514592933947772657450076805	decimal
    //[7]	0.5684931506849315068493150685	decimal
    //[8]	0.619047619047619047619047619	decimal
    //[9]	0.5416666666666666666666666667	decimal
    //[10]	0.6363636363636363636363636364	decimal
    //[11]	0.375	decimal
    //[12]	1	decimal

    // PercentageOfCasesEndedHere
    //[0]	0.5362817403355937237044003366	decimal
    //[1]	0.2507300895906548532396178785	decimal
    //[2]	0.1187447408800673167351383458	decimal
    //[3]	0.052356085729842102658021086	decimal
    //[4]	0.0233999901004801267138543781	decimal
    //[5]	0.0104316190664752759491164679	decimal
    //[6]	0.0044424095431371578478443795	decimal
    //[7]	0.0020541503737068752165519972	decimal
    //[8]	0.0009652031876453991981388903	decimal
    //[9]	0.0003217343958817997327129634	decimal
    //[10]	0.0001732415977825075483839034	decimal
    //[11]	0.000037123199524823046082265	decimal
    //[12]	0.0000618719992080384101371084	decimal


    public class Test
    {
        public decimal ProbabilityOfGreenNextDay { get; internal set; }
        public decimal PercentageOfCasesEndedHere { get; internal set; }
    }
}
