﻿using System.Security.Claims;
using Api.EntityFramework.Context;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Api.EntityFramework.Entities;
using Api.ReactModels.Account;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly SqlDbContext _context;

        public AccountController(SqlDbContext context)
        {
            _context = context;
        }

        [HttpPost("create")]
        public async Task<ActionResult> CreateUser([FromBody] CreateAccountModel createModel)
        {
            var userEntity = new UserEntity
            {
                Username = createModel.Username,
                HashedPassword = BCrypt.Net.BCrypt.HashPassword(createModel.Password)
            };

            await _context.Users.AddAsync(userEntity);
            await _context.SaveChangesAsync();
            return Ok();
        }
        
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginModel loginModel)
        {
            if (User.Identity.IsAuthenticated)
            {
                return Ok();
            }

            var user = _context.Users.FirstOrDefault(u => u.Username == loginModel.Username);
            if (user == null)
            {
                return Unauthorized();
            }

            if (!BCrypt.Net.BCrypt.Verify(loginModel.Password, user.HashedPassword))
            {
                return Unauthorized();
            }

            var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
            identity.AddClaim(new Claim("Name", "Fredrik"));

            var principal = new ClaimsPrincipal(identity);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

            return Accepted();
        }

        [HttpPost("logout")]
        public async Task<IActionResult> Logout()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return BadRequest();
            }
            
            await HttpContext.SignOutAsync();
            return Ok();
        }
    }
}
