﻿namespace Api.Controllers
{
    public class BacktestRequest
    {
        public List<string> Symbols { get; set; }
        public string StrategyName { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
}