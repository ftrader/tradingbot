﻿using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using Api.ReactModels.Strategy;
using Api.Strategies.Signal;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StrategyController : ControllerBase
    {
        public StrategyController()
        {
        }
        
        [HttpGet("getallstrategies")]
        public List<StrategyModel> Get()
        {
            var assembly = Assembly.GetEntryAssembly();

            var strategyModels = new List<StrategyModel>();
            foreach (var definedType in assembly.DefinedTypes)
            {
                if (definedType.BaseType == typeof(TradingStrategyBase))
                {
                    var strategyType = Type.GetType(definedType.FullName);
                    var strategyBaseConstructor = strategyType.GetConstructor(Type.EmptyTypes);
                    var objectConstructor = strategyBaseConstructor.Invoke(new object[]{});

                    var method = strategyType.GetMethod("GetName");
                    var strategyName = method.Invoke(objectConstructor, null) as string;

                    strategyModels.Add(new StrategyModel(strategyName, definedType.FullName, definedType.GUID));
                }
            }

            return strategyModels;
        }
    }
}
