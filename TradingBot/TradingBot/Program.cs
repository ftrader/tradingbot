using System.Reflection;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Api.Domain.Yahoo;
using Api.EntityFramework.Context;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddLazyCache();
builder.Services.AddMediatR(Assembly.GetExecutingAssembly());

// TODO: Currently allowing everyone to access. Limit to APP.
builder.Services.AddCors(o => o.AddPolicy("AllowAll", builder =>
{
    builder.AllowAnyOrigin()
        .AllowAnyMethod()
        .AllowAnyHeader();
}));


//var connectionString = "Server=localhost\\SQLEXPRESS;Initial Catalog=trader;Integrated Security=SSPI;TrustServerCertificate=True";
//builder.Services.AddDbContext<SqlDbContext>(options => options.UseSqlServer(connectionString));
var server = "192.168.1.71";
var port = "5432";
var name = "postgres";
var user = "postgres";
var password = "postgrespw";


builder.Services.AddDbContext<SqlDbContext>();
AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
//Username = postgres; Password = postgres; Server = db; Database = gadget
builder.Services.AddScoped<IDownloadTicketService, YahooQuotesService>();

var app = builder.Build();



// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// TODO: Fix so it doesn't allow everything
app.UseCors("AllowAll");

app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.Run();