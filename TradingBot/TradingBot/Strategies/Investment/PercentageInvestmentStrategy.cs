﻿using Api.Domain.Backtest;
using Api.EntityFramework.Entities;

namespace Api.Strategies.Investment
{
    public class PercentageInvestmentStrategy : InvestmentStrategy
    {
        private readonly int _percentagePerTrade;

        public PercentageInvestmentStrategy(int percentagePerTrade = 10)
        {
            _percentagePerTrade = percentagePerTrade;
        }
        
        public override int GetQuantityToBuy(TransactionLog log, Candle candle)
        {
            var percentToInvest = _percentagePerTrade;
            var amountToBuy = log.CurrentTotalAmount() * percentToInvest / 100m;
            var quantityToBuy = (int) Math.Floor(amountToBuy / candle.Close);
            return quantityToBuy;
        }

        public override int GetQuantityToSell(TransactionLog log, Candle candle)
        {
            var availableQuantity = log.CurrentPositions
                .Where(x => x.Entry.Symbol == candle.Symbol)
                .Sum(x => x.Quantity);
            
            return availableQuantity;
        }
    }
}
