﻿using Api.Domain.Backtest;
using Api.EntityFramework.Entities;

namespace Api.Strategies.Investment
{
    public class MainInvestmentStrategy : InvestmentStrategy
    {
        public override int GetQuantityToBuy(TransactionLog log, Candle candle)
        {
            var currentlyInvested = log.CurrentInvestmentInStock(candle.Symbol);
            var percentageInvested = currentlyInvested / (log.Balance + currentlyInvested);
            if (percentageInvested > 0.2m)
            {
                // Don't invest more than 20% in a stock
                return 0;
            }

            var percentToInvest = 5;
            var amountToBuy = log.CurrentTotalAmount() * percentToInvest / 100;
            var quantityToBuy = (int) Math.Floor(amountToBuy / candle.Close);
            return quantityToBuy;
        }

        public override int GetQuantityToSell(TransactionLog log, Candle candle)
        {
            var availableQuantity = log.CurrentPositions
                .Where(x => x.Entry.Symbol == candle.Symbol)
                .Sum(x => x.Quantity);
            
            return availableQuantity;
        }
    }
}
