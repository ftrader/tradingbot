﻿using Api.Domain.Backtest;
using Api.EntityFramework.Entities;

namespace Api.Strategies.Investment
{
    public abstract class AllocationMethod
    {
        public abstract int GetQuantityToBuy(TransactionLog log, Candle candle);
        public abstract int GetQuantityToSell(TransactionLog log, Candle candle);
    }
}