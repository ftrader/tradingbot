﻿using Api.Domain.Backtest;
using Api.EntityFramework.Entities;

namespace Api.Strategies.Investment
{
    public class EverythingAllocationMethod : AllocationMethod
    {
        public override int GetQuantityToBuy(TransactionLog log, Candle candle)
        {
            return int.MaxValue; // All the stocks
        }

        public override int GetQuantityToSell(TransactionLog log, Candle candle)
        {
            return log.QuantityFor(candle.Symbol);
        }
    }
}
