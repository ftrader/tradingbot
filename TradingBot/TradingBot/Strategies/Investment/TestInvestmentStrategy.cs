﻿using Api.Domain.Backtest;
using Api.EntityFramework.Entities;

namespace Api.Strategies.Investment
{
    public class TestInvestmentStrategy : InvestmentStrategy
    {
        public override int GetQuantityToBuy(TransactionLog log, Candle candle)
        {
            return 10;
        }

        public override int GetQuantityToSell(TransactionLog log, Candle symbol)
        {
            return 10;
        }
    }
}