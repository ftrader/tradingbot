﻿using Api.ReactModels.Graph;

namespace Api.Strategies.Signal
{
    public class DebugStrategy : TradingStrategyBase
    {
        public DebugStrategy()
        {
            
        }
        
        public override string GetName()
        {
            return "Debug strategy - Buy Mon - Sell Friday";
        }

        public override bool BuySignal()
        {
            if (Today.Date == new DateTime(2019, 1, 2))
            {
                return true;
            }

            return false;
//            return candles.Last().Date.DayOfWeek == DayOfWeek.Monday;
        }
        
        public override bool SellSignal()
        {
            if (Today.Date == new DateTime(2019, 12, 31))
            {
                return true;
            }

            return false;
//            return candles.Last().Date.DayOfWeek == DayOfWeek.Friday;
        }


        public override List<GraphData> GetSignalGraphs()
        {
            return new List<GraphData>();
        }

        public override TradingStrategyBase CopyInstance()
        {
            return new DebugStrategy();
        }

        public override bool UseUniqueInstance()
        {
            return true;
        }
    }
}