﻿using Api.ReactModels.Graph;
using Api.Utils;

namespace Api.Strategies.Signal
{
    public class MeanRevertingStrategy : TradingStrategyBase
    {
        private decimal _alpha;
        private decimal _ema;
        
        private readonly List<Tick> _lowerBoundTicks = new();
        private readonly List<Tick> _upperBoundTicks = new();
        private readonly List<Tick> _200Ma = new();
        private readonly List<Tick> _rsiTicks = new();

        private const decimal UpperPercentageBoundary = 1.02m;
        private const decimal LowerPercentageBoundary = 0.98m;

        public MeanRevertingStrategy()
        {
            SetValues(15);
        }

        public MeanRevertingStrategy(int lower)
        {
            SetValues(lower);
        }

        private void SetValues(int lower)
        {
            _alpha = 2m / (lower + 1m);
        }

        public override string GetName()
        {
            return "Mean reverting";
        }

        protected override void CalculateVariables()
        {
            _200Ma.Add(new Tick(Today.Date, IndicatorUtils.MovingAverage(Candles, 200)));

            _ema = IndicatorUtils.ExponentialMovingAverage(Candles, _ema, _alpha);
            _lowerBoundTicks.Add(new Tick(Today.Date, _ema * LowerPercentageBoundary));
            _upperBoundTicks.Add(new Tick(Today.Date, _ema * UpperPercentageBoundary));

            var rsi = IndicatorUtils.RSI(Candles, 15);
            _rsiTicks.Add(new Tick(Today.Date, rsi));
        }

        public override bool BuySignal()
        {
            if (PositionOpen)
            {
                return false;
            }

            // long upward trend
            if (_200Ma.Last().Close > Today.Close)
            {
                return false;
            }
            
            var lowerBoundary = _lowerBoundTicks.Last().Close;
            if (Today.Close < lowerBoundary)
            {
                return Buy();
            }
            return false;
        }
        
        public override bool SellSignal()
        {
            var upperBoundary = _upperBoundTicks.Last().Close;
            if (Today.Close >= upperBoundary)
            {
                return Sell();
            }

            //if (StopLossExceeded(10))
            //{
            //    return Sell();
            //}
            
            return false;
        }

        public override List<GraphData> GetSignalGraphs()
        {
            return new List<GraphData>
            { 
                new GraphData("Lower bound " + Today.Symbol, _lowerBoundTicks),
                new GraphData("Upper bound " + Today.Symbol, _upperBoundTicks),
                new GraphData("200 MA " + Today.Symbol, _200Ma), 
            };
        }

        public override GraphData GetRSIGraph()
        {
            return new GraphData("RSI " + Today.Symbol, _rsiTicks);
        }

        public override TradingStrategyBase CopyInstance()
        {
            return new MeanRevertingStrategy();
        }

        public override bool UseUniqueInstance()
        {
            return true;
        }
    }
}