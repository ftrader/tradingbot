﻿using Api.ReactModels.Graph;
using Api.Utils;

namespace Api.Strategies.Signal
{
    public class ExponentialMovingAverageStrategy : TradingStrategyBase
    {
        private int _lower;
        private int _upper;
        private decimal _lowerAlpha;
        private decimal _upperAlpha;
        private decimal _lowerEma;
        private decimal _upperEma;

        private readonly List<Tick> _lowerTicks = new List<Tick>();
        private readonly List<Tick> _upperTicks = new List<Tick>();
        
        public ExponentialMovingAverageStrategy()
        {
            SetValues(12, 26);
        }

        public ExponentialMovingAverageStrategy(int lower, int upper)
        {
            SetValues(lower, upper);
        }

        private void SetValues(int lower, int upper)
        {
            _lower = lower;
            _upper = upper;
            _lowerAlpha = 2m / (lower + 1m);
            _upperAlpha = 2m / (upper + 1m);
        }

        public override string GetName()
        {
            return "Exponential Moving Average: <" + _lower + ", " + _upper + "> days";
        }

        public override bool BuySignal()
        {
            _lowerEma = IndicatorUtils.ExponentialMovingAverage(Candles, _lowerEma, _lowerAlpha);
            _upperEma = IndicatorUtils.ExponentialMovingAverage(Candles, _upperEma, _upperAlpha);
            
            var date = Candles.Last().Date;
            _lowerTicks.Add(new Tick(date, _lowerEma));
            _upperTicks.Add(new Tick(date, _upperEma));
            
            return _lowerEma > _upperEma;
        }
        
        public override bool SellSignal()
        {
            return !BuySignal();
        }

        public override List<GraphData> GetSignalGraphs()
        {
            return new List<GraphData>
            {
                new GraphData("Lower EMA " + _lower, _lowerTicks), 
                new GraphData("Upper EMA" + _upper, _upperTicks)
            };
        }

        public override TradingStrategyBase CopyInstance()
        {
            return new ExponentialMovingAverageStrategy(_lower, _upper);
        }

        public override bool UseUniqueInstance()
        {
            return true;
        }
    }
}
