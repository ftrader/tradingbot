﻿using Api.ReactModels.Graph;
using Api.Utils;

namespace Api.Strategies.Signal
{
    public class MovingAverageStrategy : TradingStrategyBase
    {
        private readonly int _lower;
        private readonly int _upper;
        private readonly List<Tick> _lowerTicks = new List<Tick>();
        private readonly List<Tick> _upperTicks = new List<Tick>();

        public MovingAverageStrategy()
        {
            _lower = 50;
            _upper = 200;
        }
        
        public MovingAverageStrategy(int lower, int upper)
        {
            _lower = lower;
            _upper = upper;
        }

        public override string GetName()
        {
            return "Moving Average: <" + _lower + ", " + _upper + "> days";
        }
        
        // Buy signal will be valid when the lower MA crosses over the upper MA
        public override bool BuySignal()
        {
            var lowerMa = IndicatorUtils.MovingAverage(Candles, _lower);
            var upperMa = IndicatorUtils.MovingAverage(Candles, _upper);
            
            var date = Today.Date;
            _lowerTicks.Add(new Tick(date, lowerMa));
            _upperTicks.Add(new Tick(date, upperMa));

            return lowerMa > upperMa;
        }
        
        public override bool SellSignal()
        {
            return !BuySignal();
        }

        public override List<GraphData> GetSignalGraphs()
        {
            return new List<GraphData>
            {
                new GraphData("Moving average lower bound", _lowerTicks),
                new GraphData("Moving average upper bound", _upperTicks)
            };
        }

        public override TradingStrategyBase CopyInstance()
        {
            return new MovingAverageStrategy(_lower, _upper);
        }

        public override bool UseUniqueInstance()
        {
            return true;
        }
    }
}
