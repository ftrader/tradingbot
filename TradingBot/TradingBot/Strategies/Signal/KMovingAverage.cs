﻿using Api.EntityFramework.Entities;
using Api.ReactModels.Graph;
using Api.Utils;

namespace Api.Strategies.Signal
{
    public class KMovingAverage : TradingStrategyBase
    {
        private readonly List<Tick> ticks = new List<Tick>();
        private int Days = 10;
        private int StreakRequired = 10;
//        private decimal _alpha = 2f / (20 + 1m);
        private decimal _movingAverage;
        
        public KMovingAverage()
        {
        }
        
        public override string GetName()
        {
            return "K moving average strategy";
        }

        public override bool BuySignal()
        {
            AddMovingAverage(Candles);
            var lastThree = ticks.TakeLast(StreakRequired).ToList();
            
            // Don't buy if we don't have three points
            if (lastThree.Count() != StreakRequired)
            {
                return false;
            }
            
            
            var lastCandles = ticks.TakeLast(StreakRequired).ToList();
            for (var i = 0; i < lastCandles.Count-1; i++)
            {
                if (lastCandles[i].Close > lastCandles[i + 1].Close)
                {
                    return false;
                }
            }

            return true;
        }
        
        public override bool SellSignal()
        {
            AddMovingAverage(Candles);
            
            var lastCandles = ticks.TakeLast(StreakRequired).ToList();
            for (var i = 0; i < lastCandles.Count-1; i++)
            {
                if (lastCandles[i].Close < lastCandles[i + 1].Close)
                {
                    return false;
                }
            }

            return true;
        }

        private void AddMovingAverage(IEnumerable<Candle> candles)
        {
            _movingAverage = IndicatorUtils.MovingAverage(candles, Days);
//            _movingAverage = CandleHelper.ExponentialMovingAverage(candles, _movingAverage, Alpha);
            ticks.Add(new Tick(candles.Last().Date, _movingAverage));
        }

        public override List<GraphData> GetSignalGraphs()
        {
            return new List<GraphData>{ new GraphData(Days + " day average", ticks)};
        }

        public override TradingStrategyBase CopyInstance()
        {
            return new KMovingAverage();
        }

        public override bool UseUniqueInstance()
        {
            return true;
        }
    }
}