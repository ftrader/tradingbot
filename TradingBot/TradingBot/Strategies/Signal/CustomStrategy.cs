﻿using Api.EntityFramework.Entities;
using Api.ReactModels.Graph;
using Api.Utils;

namespace Api.Strategies.Signal
{
    public class CustomStrategy : TradingStrategyBase
    {
        private readonly List<Tick> ticks = new List<Tick>();
        private int Days = 15;
        private int HoldDays = 3;
        private int backDays = 3;
        private DateTime LastAction = new DateTime(1,1,1);
        public CustomStrategy()
        {
            
        }
        
        public override string GetName()
        {
            return "Custom strategy";
        }

        // Buy when 20 MA is going up. Sell when k = 0
        public override bool BuySignal()
        {
            // Must hold for 5 days.
            if ((Candles.Last().Date - LastAction).TotalDays < HoldDays)
            {
                return false;
            }
            
            ticks.Add(new Tick(Candles.Last().Date, IndicatorUtils.MovingAverage(Candles, Days)));
            if (Candles.ToList().FirstOrDefault(x => x.Date < Candles.Last().Date.AddDays(-Days)) == null)
            {
                return false;
            }
            
            var kValue = GetKValue(Candles);
            if (kValue >= 1.03m)
            {
                // Buying
                LastAction = Today.Date;
                return true;
            }
            return false;
        }
        
        public override bool SellSignal()
        {
            // Must hold for 5 days.
            if ((Today.Date - LastAction).TotalDays < HoldDays)
            {
                return false;
            }
            
            ticks.Add(new Tick(Today.Date, IndicatorUtils.MovingAverage(Candles, Days)));
            var kValue = GetKValue(Candles);

            if (kValue < 1m)
            {
                // Selling
                LastAction = Today.Date;
                return true;
            }
            return false;
        }

        private decimal GetKValue(IEnumerable<Candle> candles)
        {
            var currentMovingAverage = IndicatorUtils.MovingAverage(candles, Days);
            var latestDate = candles.Last().Date.AddDays(-backDays);
            
            var oldCandles = candles.Where(x => x.Date < latestDate);
            var MovingAverage20DaysAgo = IndicatorUtils.MovingAverage(oldCandles, Days);
            
            return currentMovingAverage / MovingAverage20DaysAgo;
        }
        

        public override List<GraphData> GetSignalGraphs()
        {
            return new List<GraphData>{ new GraphData("20 day average", ticks) };
        }

        public override TradingStrategyBase CopyInstance()
        {
            return new CustomStrategy();
        }

        public override bool UseUniqueInstance()
        {
            return true;
        }
    }
}