﻿using Api.EntityFramework.Entities;
using Api.ReactModels.Graph;

namespace Api.Strategies.Signal
{
    public abstract class TradingStrategyBase
    {
        protected List<Candle> Candles { get; private set; } = new();
        
        protected Candle Today { get; private set; }
        private decimal EntryPrice { get; set; }
        private decimal HighestPrice { get; set; }
        protected bool PositionOpen { get; private set; } 

        public void Update(List<Candle> candles)
        {
            Candles = candles;
            Today = candles.Last();
            
            CalculateVariables();
        }

        public void AddCandle(Candle candle)
        {
            Candles.Add(candle);
            Today = candle;

            CalculateVariables();
        }

        public abstract string GetName(); // Used by reflection
        public abstract TradingStrategyBase CopyInstance();
        public abstract bool UseUniqueInstance();

        protected virtual void CalculateVariables()
        {
            if (PositionOpen)
            {
                HighestPrice = Today.Close >= HighestPrice 
                    ? Today.Close 
                    : HighestPrice;

            }
            CalculateVariablesForStrategy();
        }

        protected virtual void CalculateVariablesForStrategy() { /* Do nothing */ }

        public abstract bool BuySignal();

        
        protected bool Sell()
        {
            PositionOpen = false;
            HighestPrice = 0;
            return true;
        }
        
        protected bool Buy()
        {
            EntryPrice = Today.Close;
            HighestPrice = Today.Close >= HighestPrice ? Today.Close : HighestPrice;
            PositionOpen = true;
            return true;
        }
        
        public abstract bool SellSignal();
        public abstract List<GraphData> GetSignalGraphs();
        public virtual GraphData GetRSIGraph() => null;

        protected bool StopLossExceeded(int percentage)
        {
            if (Today.Close < HighestPrice * (1m - percentage / 100m))
            {
                return true;
            }

            return false;
        }
    }
}
