﻿using Api.ReactModels.Graph;
using Api.Utils;

namespace Api.Strategies.Signal
{
    public class SimpleAverageStrategy : TradingStrategyBase
    {
        private decimal _rsi;
        private decimal _longMa;
        private decimal _shortMa;
        private readonly int _days;
        private readonly List<Tick> _shortTicks = new List<Tick>();
        private readonly List<Tick> _longTicks = new List<Tick>();
        private readonly List<Tick> _rsiTicks = new List<Tick>();

        public SimpleAverageStrategy()
        {
            _days = 20;
        }
        
        public SimpleAverageStrategy(int days)
        {
            _days = days;
        }

        public override string GetName()
        {
            return "RSI: " + _days + " days";
        }

        protected override void CalculateVariables()
        {
            var date = Today.Date;
            _longMa = IndicatorUtils.MovingAverage(Candles, 200);
            _shortMa = IndicatorUtils.MovingAverage(Candles, _days);
            _rsi = IndicatorUtils.RSI(Candles, 14);
            
            _longTicks.Add(new Tick(date, _longMa));
            _shortTicks.Add(new Tick(date, _shortMa));
            _rsiTicks.Add(new Tick(date, _rsi));
        }

        public override bool BuySignal()
        {
            // Rule 1
            if (Today.Close < _longMa)
            {
                return false;
            }

            return _rsi < 30m;
        }
        
        public override bool SellSignal()
        {
            return _rsi > 75m;
        }

        
        // Return rsi values.
        public override List<GraphData> GetSignalGraphs()
        {
            return new List<GraphData>
            {
                new GraphData("200 MA", _longTicks),
                new GraphData(_days + " MA", _shortTicks)
            };
        }

        public override GraphData GetRSIGraph()
        {
            return new GraphData(_days + " RSI", _rsiTicks);
        }

        public override TradingStrategyBase CopyInstance()
        {
            return new SimpleAverageStrategy();
        }

        public override bool UseUniqueInstance()
        {
            return true;
        }
    }
}