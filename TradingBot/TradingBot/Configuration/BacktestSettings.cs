﻿namespace Api.Configuration
{

    public interface ISettings
    {
        decimal TransactionFee { get; set; }
    }

    public class BacktestSettings : ISettings
    {
        public decimal TransactionFee { get; set; } = 0.003m;
    }
}