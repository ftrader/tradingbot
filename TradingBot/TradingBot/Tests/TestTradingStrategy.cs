﻿using Api.ReactModels.Graph;
using Api.Strategies.Signal;

namespace Api.Tests
{
    public class TestTradingStrategy : TradingStrategyBase
    {
        public TestTradingStrategy()
        {
        }
        
        public override string GetName()
        {
            return "Buy on odd days, sell on even days.";
        }

        public override bool BuySignal()
        {
            return Today.Date.Day % 2 == 1;
        }
        
        public override bool SellSignal()
        {
            return Today.Date.Day % 2 == 0;
        }

        public override List<GraphData> GetSignalGraphs()
        {
            return new List<GraphData>();
        }

        public override TradingStrategyBase CopyInstance()
        {
            return new TestTradingStrategy();
        }

        public override bool UseUniqueInstance()
        {
            return true;
        }
    }
}