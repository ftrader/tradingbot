﻿using NUnit.Framework;
using Api.Domain.Backtest;
using Api.EntityFramework.Entities;

namespace Api.Tests
{
    [TestFixture]
    public class LogTests
    {
        [Test]
        public void Buy_and_sell_without_price_change()
        {
            var startBalance = 100;
            var transactionFee = 0.01m;
            var log = new TransactionLog(startBalance, transactionFee);

            var quantity = 10;
            var stock = new Candle("testsymbol", 1);

            // two actions
            log.Buy(stock, quantity); // cost 1 fee
            log.Sell(stock, quantity); // cost 1 fee
            
            var totalFee = 2 * quantity * transactionFee;
            
            Assert.AreEqual(startBalance - totalFee, log.Balance);
        }
        
        [Test]
        public void Buy_and_sell_double_price_increase_no_fee()
        {
            var log = new TransactionLog(100, 0);

            var buyCandle = new Candle("testsymbol", 1);
            var sellCandle = new Candle("testsymbol", 2);

            var quantity = 10;
            log.Buy(buyCandle, quantity); // cost 1 fee
            log.Sell(sellCandle, quantity); // cost 1 fee
            
            
            Assert.AreEqual(110m, log.Balance);
            Assert.AreEqual(10m, log.BalancePercentageChange());
        }
        
        [Test]
        public void Buy_and_sell_half_price_increase_no_fee()
        {
            var log = new TransactionLog(100, 0);

            var buyCandle = new Candle("testsymbol", 1);
            var sellCandle = new Candle("testsymbol", 0.5m);

            var quantity = 10;
            log.Buy(buyCandle, quantity); // cost 1 fee
            log.Sell(sellCandle, quantity); // cost 1 fee
            
            Assert.AreEqual(95m, log.Balance);
            Assert.AreEqual(-5m, log.BalancePercentageChange());
        }
        
        [Test]
        public void Buy_once_sell_twice_results_in_two_positions()
        {
            var log = new TransactionLog(100, 0);

            var day1 = new Candle { Close = 1, Date = new DateTime(2000, 1, 1), Symbol = "testsymbol" };
            var day2 = new Candle { Close = 2, Date = new DateTime(2000, 1, 2), Symbol = "testsymbol" };
            var day3 = new Candle { Close = 2, Date = new DateTime(2000, 1, 3), Symbol = "testsymbol" };

            log.Buy(day1, 4);
            log.Sell(day2, 2);
            log.Sell(day3, 2);
            
            Assert.AreEqual(2, log.ClosedPositions.Count);
            Assert.AreEqual(3, log.TotalTrades);
            Assert.AreEqual(100, log.CalculateWinRate());
            Assert.AreEqual(new DateTime(2000, 1, 1), log.ClosedPositions[0].Entry.Date);
            Assert.AreEqual(new DateTime(2000, 1, 2), log.ClosedPositions[0].Exit.Date);
            Assert.AreEqual(new DateTime(2000, 1, 1), log.ClosedPositions[1].Entry.Date);
            Assert.AreEqual(new DateTime(2000, 1, 3), log.ClosedPositions[1].Exit.Date);
        }
        
        [Test]
        public void Buy_twice_sell_once_results_in_two_positions()
        {
            var log = new TransactionLog(100, 0);

            var day1 = new Candle { Close = 2, Date = new DateTime(2000, 1, 1), Symbol = "testsymbol" };
            var day2 = new Candle { Close = 1, Date = new DateTime(2000, 1, 2), Symbol = "testsymbol" };
            var day3 = new Candle { Close = 1, Date = new DateTime(2000, 1, 3), Symbol = "testsymbol" };

            log.Buy(day1, 2);
            log.Buy(day2, 2);
            log.Sell(day3, 4);
            
            Assert.AreEqual(2, log.ClosedPositions.Count);
            Assert.AreEqual(3, log.TotalTrades);
            Assert.AreEqual(0, log.CalculateWinRate());
            Assert.AreEqual(new DateTime(2000, 1, 1), log.ClosedPositions[0].Entry.Date);
            Assert.AreEqual(new DateTime(2000, 1, 3), log.ClosedPositions[0].Exit.Date);
            Assert.AreEqual(new DateTime(2000, 1, 2), log.ClosedPositions[1].Entry.Date);
            Assert.AreEqual(new DateTime(2000, 1, 3), log.ClosedPositions[1].Exit.Date);
        }
        
        
        [Test]
        public void Test_multiple_statistics()
        {
            var log = new TransactionLog(1000, 0.1m);

            log.Buy(new Candle { Close = 10, Date = new DateTime(2000, 1, 1), Symbol = "testsymbol" }, 2); 
            log.Buy(new Candle { Close = 20, Date = new DateTime(2000, 1, 2), Symbol = "testsymbol" }, 2);
            log.Sell(new Candle { Close = 10, Date = new DateTime(2000, 1, 3), Symbol = "testsymbol" }, 4);
            log.Buy(new Candle { Close = 10, Date = new DateTime(2000, 1, 4), Symbol = "testsymbol2" }, 4);
            log.Sell(new Candle { Close = 1, Date = new DateTime(2000, 1, 5), Symbol = "testsymbol2" }, 4);
            log.Buy(new Candle { Close = 10, Date = new DateTime(2000, 1, 6), Symbol = "testsymbol2" }, 4);
            log.Sell(new Candle { Close = 10, Date = new DateTime(2000, 1, 7), Symbol = "testsymbol2" }, 4);

            var fees = 0.1m * (10*2 + 20*2 + 10*4 + 10*4 + 1*4 + 10*4 + 10*4);
            var cost = 10*2 + 20*2 - 10*4 + 10*4 - 1*4 + 10*4 - 10*4;
            var stats = log.GetStatistics();
            var calculatedEndBalance = 1000 - cost - fees;
            
            Assert.AreEqual(fees, stats.TotalTransactionFeeCost);
            Assert.AreEqual(calculatedEndBalance, stats.Balance);
            Assert.AreEqual(-7.84m, stats.PercentageChange);
            Assert.AreEqual(- cost - fees, stats.PriceChange);
            Assert.AreEqual(0, stats.MaximumDrawdown);
        }
        
        [Test]
        public void Test_max_drawdown()
        {
            var log = new TransactionLog(1000, 0);

            var list = new System.Collections.Generic.List<Candle>
            {
                new Candle {Close = 100, Date = new DateTime(2000, 1, 1), Symbol = "testsymbol"}, // 900 + 100
                new Candle {Close = 50, Date = new DateTime(2000, 1, 2), Symbol = "testsymbol"}, // 950
                new Candle {Close = 500, Date = new DateTime(2000, 1, 3), Symbol = "testsymbol"}, // 450 + 500
                new Candle {Close = 100, Date = new DateTime(2000, 1, 4), Symbol = "testsymbol"}, // 550
                new Candle {Close = 550, Date = new DateTime(2000, 1, 5), Symbol = "testsymbol"}, // 0 + 550
                new Candle {Close = 1500, Date = new DateTime(2000, 1, 6), Symbol = "testsymbol"}, // 1500
                new Candle {Close = 1500, Date = new DateTime(2000, 1, 7), Symbol = "testsymbol"}, // 0 + 1500
                new Candle {Close = 750, Date = new DateTime(2000, 1, 8), Symbol = "testsymbol"} // 750
            };

            var drawDowns = new System.Collections.Generic.List<decimal>();
            for (var i = 0; i < 8; i+=2)
            {
                log.CalculateForStock(list[i]);
                log.Buy(list[i], 1);
                log.CalculateForEndOfDay(list[i].Date);
                
                log.CalculateForStock(list[i+1]);
                log.Sell(list[i+1], 1);
                log.CalculateForEndOfDay(list[i+1].Date);
                drawDowns.Add(log.GetStatistics().MaximumDrawdown);
            }
            
            Assert.AreEqual(5, drawDowns[0]);
            Assert.AreEqual(45, drawDowns[1]);
            Assert.AreEqual(45, drawDowns[2]);
            Assert.AreEqual(50, drawDowns[3]);
        }
    }
}