﻿using NUnit.Framework;
using Api.Domain.Backtest;
using Api.EntityFramework.Entities;
using Api.Strategies.Investment;

namespace Api.Tests
{

    [TestFixture]
    public class BacktestTests
    {

        [Test]
        public void Backtest_makes_4_trades()
        {
            var dict = new Dictionary<string, List<Candle>>
            {
                ["testsymbol"] = new List<Candle>
                {
                    new Candle {Close = 1, Date = new DateTime(2000, 1, 1), Symbol = "testsymbol"},
                    new Candle {Close = 2, Date = new DateTime(2000, 1, 2), Symbol = "testsymbol"}, // 1
                    new Candle {Close = 1, Date = new DateTime(2000, 1, 3), Symbol = "testsymbol"},
                    new Candle {Close = 2, Date = new DateTime(2000, 1, 4), Symbol = "testsymbol"}, // 1
                    new Candle {Close = 1, Date = new DateTime(2000, 1, 5), Symbol = "testsymbol"},
                    new Candle {Close = 2, Date = new DateTime(2000, 1, 6), Symbol = "testsymbol"}, // 1
                    new Candle {Close = 1, Date = new DateTime(2000, 1, 7), Symbol = "testsymbol"},
                    new Candle {Close = 2, Date = new DateTime(2000, 1, 8), Symbol = "testsymbol"} // 1
                }
            };

            // Test strategy always buys at Close == 1 and sells at Close == 2.
            var tradingStrategy = new TestTradingStrategy();
            
            // InvestmentStrategy only buys/sells 10 stock at a time
            var investmentStrategy = new TestInvestmentStrategy();
            var backtest = new Backtest();
            backtest.AddStocksAndStrategies(dict, tradingStrategy, investmentStrategy);
            backtest.Configure(10, 0m);
            backtest.Execute();
            var log = backtest.GetLog();
            
            Assert.AreEqual(50, log.Balance);
            Assert.AreEqual(4, log.ClosedPositions.Count);
        }
        
        
        [Test]
        public void Backtest_does_not_buy_more_than_it_can()
        {
            var dict = new Dictionary<string, List<Candle>>
            {
                ["testsymbol"] = new List<Candle>
                {
                    new Candle {Close = 1, Date = new DateTime(2000, 1, 1), Symbol = "testsymbol"},
                    new Candle {Close = 2, Date = new DateTime(2000, 1, 2), Symbol = "testsymbol"},
                }
            };

            var tradingStrategy = new TestTradingStrategy();
            var investmentStrategy = new TestInvestmentStrategy();
            var backtest = new Backtest();

            backtest.AddStocksAndStrategies(dict, tradingStrategy, investmentStrategy);
            backtest.Configure(1, 0m);
            backtest.Execute();
            
            var log = backtest.GetLog();
            
            Assert.AreEqual(2, log.Balance);
            Assert.AreEqual(1, log.ClosedPositions.Count);
        }
        
        
        [Test]
        public void Backtest_trading_fee_hundred_percent()
        {
            var dict = new Dictionary<string, List<Candle>>
            {
                ["testsymbol"] = new List<Candle>
                {
                    new Candle {Close = 1, Date = new DateTime(2000, 1, 1), Symbol = "testsymbol"},
                    new Candle {Close = 2, Date = new DateTime(2000, 1, 2), Symbol = "testsymbol"}
                }
            };


            var tradingStrategy = new TestTradingStrategy();
            var investmentStrategy = new TestInvestmentStrategy();
            var backtest = new Backtest();

            backtest.AddStocksAndStrategies(dict, tradingStrategy, investmentStrategy);
            backtest.Configure(2, 1);
            backtest.Execute();
            
            var sum = 2 - (1 * 1) - 1; // fee 100% of value 1
            sum = sum + (1 * 2) - 2; // fee 100% of value 2 

            Assert.AreEqual(sum, backtest.GetLog().Balance);
            Assert.AreEqual(3, backtest.GetLog().TotalTransactionFeeCost);
        }

        
        [Test]
        public void Backtest_trading_fee_ten_percent()
        {
            var dict = new Dictionary<string, List<Candle>>
            {
                ["testsymbol"] = new List<Candle>
                {
                    new Candle {Close = 10, Date = new DateTime(2000, 1, 1), Symbol = "testsymbol"},
                    new Candle {Close = 15, Date = new DateTime(2000, 1, 2), Symbol = "testsymbol"}
                }
            };

            var tradingStrategy = new TestTradingStrategy();
            var investmentStrategy = new TestInvestmentStrategy();
            var backtest = new Backtest();

            backtest.AddStocksAndStrategies(dict, tradingStrategy, investmentStrategy);
            backtest.Configure(1000, 0.1m);
            backtest.Execute();
            
            var sum = 1000 - (10 * 10) - 10m; // 890
            sum = sum + (10 * 15) - 15m; // 1025
                  
            Assert.AreEqual(sum, backtest.GetLog().Balance);
        }
    }
}