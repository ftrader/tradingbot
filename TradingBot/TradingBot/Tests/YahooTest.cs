﻿using Api.Domain.Yahoo;
using NUnit.Framework;

namespace Api.Tests;

[TestFixture]
public class YahooTest
{
    [Test]
    public async Task Can_get_msft_security()
    {
        var security = await YahooClient.GetSecurityAsync("msft");
        Assert.NotNull(security);
        Assert.AreEqual("Microsoft", security.DisplayName);
        Assert.AreEqual("Microsoft Corporation", security.ShortName);
    }

    [Test]
    public async Task Can_get_candles()
    {
        var candles = await YahooClient.GetHistoricalAsync("msft", new DateTime(2022, 6, 1));

        Assert.True(candles.Length > 0);
    }

    [Test]
    public void Can_not_get_candles_from_random_symbol()
    {
        Assert.ThrowsAsync<SecurityNotAvailableException>(async () =>
            {
                await YahooClient.GetHistoricalAsync("randomsymbol", new DateTime(DateTime.Now.Year, 8, 1));
            });
    }

    [Test]
    public async Task Can_not_get_candles_from_next_year()
    {
        var today = new DateTime(2022, 7, 3); // This is a sunday.
        var candles = await YahooClient.GetHistoricalAsync("msft", today);
        Assert.True(candles.Length > 0);
    }
}
