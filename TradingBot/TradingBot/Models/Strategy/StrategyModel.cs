﻿namespace Api.ReactModels.Strategy
{
    public class StrategyModel
    {
        public StrategyModel(string name, string strategy, Guid id)
        {
            Name = name;
            Strategy = strategy;
            Id = id;
        }

        public string Name { get; set; }
        public string Strategy { get; set; }
        public Guid Id { get; set; }
    }
}