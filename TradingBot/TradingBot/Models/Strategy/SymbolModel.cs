﻿using Api.EntityFramework.Entities;

namespace Api.ReactModels.Strategy
{
    public class SymbolModel
    {
        public SymbolModel(SymbolEntity symbolEntity)
        {
            Name = symbolEntity.Name;
            Symbol = symbolEntity.Symbol;
            LastFetched = symbolEntity.LastFetched;
            HasData = symbolEntity.HasData;
        }

        public string Name { get; }
        public string Symbol { get; }
        public DateTime LastFetched { get; }
        public bool HasData { get; }
    }
}