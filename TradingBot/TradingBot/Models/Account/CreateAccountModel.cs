﻿namespace Api.ReactModels.Account
{
    public class CreateAccountModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
