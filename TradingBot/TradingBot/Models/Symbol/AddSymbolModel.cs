﻿namespace Api.ReactModels.Symbol
{
    public class AddSymbolModel
    {
        public string Name { get; set; }
        public string Symbol { get; set; }
    }
}