﻿namespace Api.ReactModels.Graph
{
    public class GetGraphModel
    {
        public List<string> Symbols { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
    }
}