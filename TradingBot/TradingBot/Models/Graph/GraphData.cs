﻿namespace Api.ReactModels.Graph
{
    public class GraphData
    {
        public GraphData(string label, IEnumerable<Tick> ticks)
        {
            Label = label;
            Ticks = ticks;
        }

        public string Label { get; }
        public IEnumerable<Tick> Ticks { get; }
    }
}
