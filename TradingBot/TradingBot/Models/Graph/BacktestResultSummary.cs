﻿using Api.Domain.Backtest;

namespace Api.ReactModels.Graph
{
    public class BacktestResultSummary
    {
        public IEnumerable<SymbolBacktestResult> SymbolBacktestResults { get; set; }
        public IEnumerable<Trade> TradePercentages { get; set; }
        public BacktestStatistics BacktestStatistics { get; set; }
        public GraphData EquityCurve { get; set; }
    }

    public class Trade
    {
        public decimal Change { get; set; }
        public int Quantity { get; set; }
        public decimal BalanceChange { get; set; }
    }
    
    public class SymbolBacktestResult
    {
        public string Symbol { get; set; }
        public GraphData BuyEvents { get; set; }
        public GraphData SellEvents { get; set; }
        public IEnumerable<GraphData> SignalCharts { get; set; }
        public GraphData RSIChart { get; set; }
    }
}
