﻿using Api.EntityFramework.Entities;

namespace Api.ReactModels.Graph
{
    public class Tick
    {
        public Tick(Candle candle)
        {
            Date = candle.Date;
            Close = candle.Close;
            Volume = candle.Volume;
            Change = candle.RChange;
        }

        public Tick(DateTime date, decimal close)
        {
            Date = date;
            Close = close;
            Volume = 0;
            Change = 0;
        }

        public DateTime Date { get; set; }
        public decimal Close { get; set; }
        public long Volume { get; set; }
        public decimal Change { get; set; }
    }
}
