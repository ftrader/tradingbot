﻿using System.ComponentModel.DataAnnotations.Schema;
using YahooQuotesApi;

namespace Api.EntityFramework.Entities
{
    public class Candle
    {
        [Column("id")]
        public int Id { get; set; } // Auto increment
        
        [Column("symbol")]
        public string Symbol { get; set; } 
        
        [Column("candle_date")]
        public DateTime Date { get; set; }
        
        [Column("open")]
        public decimal Open { get; set; }
        
        [Column("high")]
        public decimal High { get; set; }
        
        [Column("low")]
        public decimal Low { get; set; }
        
        [Column("close")]
        public decimal Close { get; set; }
        
        [Column("volume")]
        public long Volume { get; set; }
        
        [Column("r_change")]
        public decimal RChange { get; set; }
        
        [Column("adj_change")]
        public decimal AdjChange { get; set; }
        
        [Column("adj_close")]
        public decimal AdjClose { get; set; }
        
        [Column("adj_low")]
        public decimal AdjLow { get; set; }
        
        [Column("adj_high")]
        public decimal AdjHigh { get; set; }
        
        [Column("adj_swing")]
        public decimal AdjSwing { get; set; }

        public Candle()
        { }
        
        public Candle(string symbol, decimal close)
        {
            Symbol = symbol;
            Close = close;
        }

        public Candle(PriceTick candle, decimal adjChange, string symbol)
        {
            var multiplier = candle.AdjustedClose / candle.Close;
            var adjHigh = multiplier * candle.High;
            var adjLow = multiplier * candle.Low;

            Symbol = symbol;
            Date = candle.Date.ToDateTimeUnspecified();
            Open = (decimal) candle.Open;
            High = (decimal) candle.High;
            Low = (decimal) candle.Low;
            Close = (decimal) candle.Close;
            Volume = candle.Volume;
            AdjChange = adjChange;
            AdjClose = (decimal) candle.AdjustedClose;
            AdjHigh = (decimal) adjHigh;
            AdjLow = (decimal) adjLow;
        }
    }
}

/*
 
DROP TABLE IF EXISTS candles;
     
CREATE TABLE candles (
    id INT identity(1,1) primary key,
    symbol varchar(31),
    candle_date DATETIME,
    [open] DECIMAL(10,3),
    [high] DECIMAL(10,3),
    [low] DECIMAL(10,3),
    [close] DECIMAL(10,3),
    [volume] bigint,
    [r_change] DECIMAL(10,3),
    [adj_change] DECIMAL(10,3),
    [adj_close] DECIMAL(10,3),
    [adj_high] DECIMAL(10,3),
    [adj_low] DECIMAL(10,3),
    [adj_swing] DECIMAL(10,3)
);

ALTER TABLE candles 
ADD CONSTRAINT unique_row 
UNIQUE (symbol, candle_date);


DROP TABLE IF EXISTS symbols;
     
CREATE TABLE symbols (
    id INT identity(1,1) PRIMARY KEY,
    name varchar(256) NOT NULL,
    short_name varchar(256) NOT NULL,
    symbol varchar(31) NOT NULL,
    last_fetched DATETIME NOT NULL,
    has_data bit NOT NULL
);

 */
