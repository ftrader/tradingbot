﻿﻿namespace Api.EntityFramework.Entities
{
    public class UserEntity
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string HashedPassword { get; set; }
    }
}

/* 

drop table users;

CREATE TABLE users (
    Id INT AUTO_INCREMENT PRIMARY KEY,
    Username varchar(31) NOT NULL,
    HashedPassword varchar(255) NOT NULL
);
*/
