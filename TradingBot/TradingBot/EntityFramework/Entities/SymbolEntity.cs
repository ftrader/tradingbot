﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Api.EntityFramework.Entities
{
    public class SymbolEntity
    {
        [Column("id")]
        public int Id { get; set; }
        
        [Column("name")]
        public string Name { get; set; }
        
        [Column("short_name")]
        public string ShortName { get; set; }

        [Column("symbol")]
        public string Symbol { get; set; }
        
        [Column("last_fetched")]
        public DateTime LastFetched { get; set; }
        
        [Column("has_data")]
        public bool HasData { get; set; }
    }
}
