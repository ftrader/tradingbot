﻿using Microsoft.EntityFrameworkCore;
using Api.EntityFramework.Entities;

namespace Api.EntityFramework.Context
{
    public class SqlDbContext : DbContext
    {
        //public SqlDbContext(DbContextOptions<SqlDbContext> options) : base(options)
        //{
        //}

        protected readonly IConfiguration Configuration;

        public SqlDbContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            // connect to postgres with connection string from app settings
            var server = "localhost";
            var port = "5432";
            var name = "postgres";
            var user = "postgres";
            var password = "postgrespw";

            var t = $"Host={server};Port={port};Database={name};Username={user};Password={password}";
            options.UseNpgsql(t);
            //options.
            //options.UseNpgsql(Configuration.GetConnectionString("WebApiDatabase"));
        }

        public DbSet<Candle> Candles { get; set; }
        public DbSet<SymbolEntity> Symbols { get; set; }
        public DbSet<UserEntity> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Candle>(entity =>
            {
                entity.ToTable("candles");
                entity.HasKey(e => e.Id);
            });

            modelBuilder.Entity<SymbolEntity>(entity =>
            {
                entity.ToTable("symbols");
                entity.HasKey(e => e.Id);
            });

            modelBuilder.Entity<UserEntity>(entity =>
            {
                entity.ToTable("users");
                entity.HasKey(e => e.Id);
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}
