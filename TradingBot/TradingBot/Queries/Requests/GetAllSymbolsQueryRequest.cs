﻿using MediatR;
using Api.ReactModels.Strategy;

namespace Api.Queries
{
    public class GetAllSymbolsQueryRequest : IRequest<List<SymbolModel>>
    { }
}