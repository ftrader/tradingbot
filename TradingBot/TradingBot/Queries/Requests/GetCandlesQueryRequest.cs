﻿using MediatR;
using Api.EntityFramework.Entities;

namespace Api.Queries
{
    public class GetCandlesQueryRequest : IRequest<List<Candle>>
    {
        public string Symbol { get; }
        public DateTime? From { get; }
        public DateTime? To { get; }
        
        public GetCandlesQueryRequest(string symbol, DateTime? from = null, DateTime? to = null)
        {
            Symbol = symbol;
            From = from;
            To = to;
        }
    }
}