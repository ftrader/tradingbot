﻿using LazyCache;
using MediatR;
using Api.EntityFramework.Entities;
using Api.Utils.Cache;
using Api.EntityFramework.Context;
using Microsoft.EntityFrameworkCore;

namespace Api.Queries
{
    public class GetCandlesQuery : IRequestHandler<GetCandlesQueryRequest, List<Candle>>
    {
        private readonly SqlDbContext _dbContext;
        private readonly IAppCache _cache;

        public GetCandlesQuery(SqlDbContext dbContext, IAppCache cache)
        {
            _dbContext = dbContext;
            _cache = cache;
        }

        public async Task<List<Candle>> Handle(GetCandlesQueryRequest request, CancellationToken cancellationToken)
        {
            Task<IEnumerable<Candle>> GetCandlesFunc() => GetCandles(request.Symbol);
            var candles = await _cache.GetOrAddAsync(CacheKey.GetCandlesQuery(request.Symbol), GetCandlesFunc);
            
            if (request.From.HasValue)
            {
                candles = candles.Where(x => x.Date >= request.From.Value);
            }

            if (request.To.HasValue)
            {
                candles = candles.Where(x => x.Date <= request.To.Value);
            }

            return candles.ToList();
        }

        private async Task<IEnumerable<Candle>> GetCandles(string symbol)
        {
            return await _dbContext.Candles
                .Where(x => x.Symbol == symbol)
                .OrderBy(x => x.Date)
                .ToListAsync();
        }
    }
}
