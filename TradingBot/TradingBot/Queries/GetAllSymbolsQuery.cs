﻿using LazyCache;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Api.ReactModels.Strategy;
using Api.Utils.Cache;
using Api.EntityFramework.Context;

namespace Api.Queries
{
    public class GetAllSymbolsQuery : IRequestHandler<GetAllSymbolsQueryRequest, List<SymbolModel>>
    {
        private readonly SqlDbContext _context;
        private readonly IAppCache _cache;

        public GetAllSymbolsQuery(SqlDbContext context, IAppCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public async Task<List<SymbolModel>> Handle(GetAllSymbolsQueryRequest request, CancellationToken cancellationToken)
        {
            return await _cache.GetOrAddAsync(CacheKey.GetSymbolsQuery, GetSymbols);
        }

        private async Task<List<SymbolModel>> GetSymbols()
        {
            var symbolEntities = await _context.Symbols.ToListAsync();
            return symbolEntities.Select(x => new SymbolModel(x)).ToList();
        }

    }
}
