CREATE TABLE IF NOT EXISTS candles (
    id serial primary key,
    symbol varchar(31),
    candle_date timestamptz,
    open numeric,
    high numeric,
    low numeric,
    close numeric,
    volume bigint,
    r_change numeric,
    adj_open numeric,
    adj_change numeric,
    adj_close numeric,
    adj_high numeric,
    adj_low numeric,
    adj_swing numeric
);

ALTER TABLE candles 
ADD CONSTRAINT unique_row 
UNIQUE (symbol, candle_date);

CREATE TABLE IF NOT EXISTS symbols (
    id serial PRIMARY KEY,
    name varchar(256) NOT NULL,
    short_name varchar(256) NOT NULL,
    symbol varchar(31) NOT NULL,
    last_fetched timestamptz NOT NULL,
    has_data Boolean NOT NULL
);
