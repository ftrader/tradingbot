﻿import React from "react";
import './Home.css';
import { withRouter } from 'react-router';

class Home extends React.Component {

    render() {
        return <div>Home yo {process.env.REACT_APP_BASE_URL}</div>;
    }
}

export { Home, withRouter };