﻿import React from "react";
import axios from "axios";
import moment, { Moment } from "moment";
import { BASE_API_URL } from "../../base";
import { StrategyPicker } from "../Strategy/StrategyPicker";


class BacktestPage extends React.Component {

    constructor(props: {}) {
        super(props);
    }

    componentDidMount(): void {
    }


    clickEvent(id: string) {
        console.log("got", id);
    }

    render() {
        return <div className="backtest">
            Backtest page.
            <StrategyPicker clickEvent={this.clickEvent} />
        </div>
    }
}

export { BacktestPage };
