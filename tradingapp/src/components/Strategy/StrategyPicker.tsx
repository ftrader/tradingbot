﻿import React from "react";
import axios from "axios";
import { BASE_API_URL } from "../../base";

interface IPickerProps {
  clickEvent: (strategyId: string) => void;
}

interface IPickerState {
  strategies: StrategyModel[];
  selectedStrategyId: string;
}

interface StrategyModel {
  name: string;
  strategy: string;
  id: string;
  properties: StrategyProperty[];
}

interface StrategyProperty {
  name: string;
  description: string;
  defaultValue: number;
}

class StrategyPicker extends React.Component<IPickerProps, IPickerState> {
  constructor(props: IPickerProps) {
    super(props);

    this.updateSelect = this.updateSelect.bind(this);
  }

  componentDidMount(): void {
    axios.get(BASE_API_URL + "/strategy/getallstrategies")
      .then(response => response.data as StrategyModel[])
      .then(response => {
        this.setState({
          strategies: response,
          selectedStrategyId: response[0].strategy
        });
      })
  }

  updateSelect(event: React.ChangeEvent<HTMLSelectElement>) {
    this.setState({
      selectedStrategyId: event.target.value
    });
  }

  render() {
    const style = {
      paddingLeft: "10px"
    };

    return (
      <div className="strategy-picker">
        <select onChange={this.updateSelect}>
          {
            this.state && this.state.strategies && this.state.strategies.map((item: StrategyModel) => {
              return <option key={item.id} value={item.strategy}>{item.name}</option>;
            })
          }
        </select>

        <div>
          {
            this.state && this.state.strategies && this.state.strategies.map((item: StrategyModel) => {
              if (!item.properties) {
                return <div />
              }
              return item.properties.map((t: StrategyProperty) => {
                return (
                  <div>
                    <label>{t.description}
                      <input className="strategy-properties" placeholder={t.description} value={t.defaultValue} />
                    </label>
                  </div>
                )
              })
            })
          }
        </div>
        <span className="strategy-picker__execute" style={style}
          onClick={() => this.props.clickEvent(this.state.selectedStrategyId)}>Execute</span>
      </div>
    );
  }
}

export { StrategyPicker };
