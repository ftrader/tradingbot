﻿import React from "react";
import './Graph.css';
import { Charts, ChartContainer, ChartRow, YAxis, LineChart, Resizable, BarChart, ScatterChart, EventMarker, Baseline, imeEvent, atTime } from "react-timeseries-charts";
import { TimeSeries, Index } from "pondjs";
import axios from "axios";
import moment from "moment";
import { BASE_API_URL, Green, Red, Black, DarkGrey, Purple } from "../../base";
import { StrategyPicker } from "../Strategy/StrategyPicker";

interface IGraphProps {
    Symbols: string[];
    From: string;
    To: string;
}

interface IGraphState {
    EquityChart: any;
    Charts: ChartModel[];
    Statistics: BacktestStatistics;

    // Global control for graphs
    mode: string;
    timeRange: any;
    showVolumeChart: boolean;
    showRsiChart: boolean;
}

interface ChartModel {
    symbol: string;
    candleSeries: any;
    volumeSeriesGreen: any;
    volumeSeriesRed: any;
    buyEvents: any;
    sellEvents: any;
    signalCharts: any[];
    rsiChart: any;

    // Marker
    tracker: Date | null;
    trackerPriceValue: string | null;
    trackerRSIValue: string | null;
    trackerEvent: any | null;
}

interface BacktestResultSummary {
    symbolBacktestResults: SymbolBacktestResult[];
    equityCurve: ChartData;
    backtestStatistics: BacktestStatistics;
}

interface SymbolBacktestResult {
    symbol: string;
    buyEvents: ChartData;
    sellEvents: ChartData;
    signalCharts: ChartData[];
    rsiChart: ChartData;
}

interface BacktestStatistics {
    maximumDrawdown: number;
    winRate: number;
    totalTrades: number;
    buyAndHoldChange: number;
    balance: number;
    priceChange: number;
    percentageChange: number;
    totalTransactionFeeCost: number;
}

interface ChartData {
    label: string;
    ticks: Tick[];
}

interface Tick {
    close: Number;
    date: Date;
    volume: number;
    change: Number;
}

interface SeriesData {
    name: string;
    columns: string[];
    points: any[][];
}

class Graph extends React.Component<IGraphProps, IGraphState> {


    componentDidMount(): void {
        this.updateGraph();
    }

    componentDidUpdate(prevProps: Readonly<IGraphProps>, prevState: Readonly<IGraphState>, snapshot?: any): void {
        if (!this.arraysEqual(prevProps.Symbols, this.props.Symbols) || prevProps.From !== this.props.From || prevProps.To !== this.props.To) {
            this.updateGraph();
        }
    }

    arraysEqual(a: string[], b: string[]) {
        if (a === b) return true;
        if (a == null || b == null) return false;
        if (a.length != b.length) return false;

        for (let i = 0; i < a.length; ++i) {
            if (a[i] !== b[i]) return false;
        }
        return true;
    }


    updateGraph() {
        const data = {
            symbols: this.props.Symbols,
            from: this.props.From,
            to: this.props.To
        };
        console.log(data);

        axios.post(BASE_API_URL + "/candle", data)
            .then(response => response.data as ChartData[])
            .then(response => {

                const parsedCharts: ChartModel[] = response.map((chart: ChartData) => {
                    const timeSeries = this.chartDataToSeries(chart);

                    const volumeDataGreen: SeriesData = {
                        name: "data",
                        columns: ["index", "volume"],
                        points: chart.ticks.filter(((x: Tick) => x.change >= 0))
                            .map((item: Tick) => {
                                return [Index.getIndexString("1d", new Date(item.date)), item.volume, item.change];
                            })
                    };

                    const volumeDataRed: SeriesData = {
                        name: "data",
                        columns: ["index", "volume"],
                        points: chart.ticks.filter(((x: Tick) => x.change < 0))
                            .map((item: Tick) => {
                                return [Index.getIndexString("1d", new Date(item.date)), item.volume, item.change];
                            })
                    };
                    return {
                        symbol: chart.label,
                        candleSeries: timeSeries,
                        volumeSeriesGreen: new TimeSeries(volumeDataGreen),
                        volumeSeriesRed: new TimeSeries(volumeDataRed),
                        signalCharts: [],
                        rsiChart: null,
                        buyEvents: undefined,
                        sellEvents: undefined,
                        tracker: null,
                        trackerPriceValue: null,
                        trackerRSIValue: null,
                        trackerEvent: null
                    };
                });

                this.setState({
                    mode: "linear",
                    timeRange: parsedCharts[0].candleSeries.timerange(),
                    showVolumeChart: false,
                    showRsiChart: false,
                    EquityChart: null,
                    Charts: parsedCharts
                });
            });
    }

    handleTimeRangeChange = (timerange: any) => {
        this.setState({ timeRange: timerange });
    };

    setModeLinear = () => {
        this.setState({ mode: "linear" });
    };

    setModeLog = () => {
        this.setState({ mode: "log" });
    };

    toggleVolumeChart = () => {
        this.setState({ showVolumeChart: !this.state.showVolumeChart });
    };

    runStrategy = (strategyName: string) => {
        const data = {
            symbols: this.props.Symbols,
            strategyName: strategyName,
            from: this.props.From,
            to: this.props.To
        };

        axios.post(BASE_API_URL + "/backtest/run", data)
            .then(response => response.data as BacktestResultSummary)
            .then(response => {
                console.log(response);
                const array = [...this.state.Charts];
                let hasRsiCharts = false;
                response.symbolBacktestResults.forEach((result: SymbolBacktestResult) => {
                    const buyEvents = this.chartDataToSeries(result.buyEvents);
                    const sellEvents = this.chartDataToSeries(result.sellEvents);
                    const signalCharts: any[] = result.signalCharts.map((chart: ChartData) => new TimeSeries(this.chartDataToSeries(chart)));
                    const rsiChart = this.chartDataToSeries(result.rsiChart);

                    const index = this.getIndex(array, result.symbol);
                    array[index].buyEvents = buyEvents;
                    array[index].sellEvents = sellEvents;
                    array[index].signalCharts = signalCharts;
                    array[index].rsiChart = rsiChart;
                    if (rsiChart) {
                        hasRsiCharts = true;
                    }
                });

                this.setState({
                    EquityChart: this.chartDataToSeries(response.equityCurve),
                    Charts: array,
                    Statistics: response.backtestStatistics,
                    showRsiChart: hasRsiCharts
                });
            });
    };

    getIndex(charts: ChartModel[], symbol: string) {
        for (let i = 0; i < charts.length; i++) {
            if (charts[i].symbol == symbol) {
                return i;
            }
        }

        return -1;
    }

    chartDataToSeries = (chartData: ChartData) => {
        if (chartData == null || chartData.ticks.length == 0) {
            return null;
        }

        const events: SeriesData = {
            name: chartData.label,
            columns: ["time", "close"],
            points: chartData.ticks.map((tick: Tick) => {
                return [moment(new Date(tick.date)).format("x"), tick.close]
            })
        };
        return new TimeSeries(events);
    };

    scatterChart = (events: any, timeRange: TimeRanges, color: string) => {
        if (!events) {
            return <ScatterChart visible={false} axis="null" series={new TimeSeries()} />;
        }

        const scatterStyle = {
            close: {
                normal: {
                    fill: color,
                    opacity: 1
                }
            }
        };

        return (
            <ScatterChart axis="y"
                columns={["close"]}
                series={events.crop(timeRange)}
                style={scatterStyle}
                radius={6} />
        );
    };

    lineChart = (series: any, timeRange: TimeRanges, color: string) => {
        if (!series || series._data == null) {
            return <LineChart visible={false} axis="null" series={new TimeSeries()} />
        }

        const croppedSeries = series.crop(timeRange);
        return (
            <LineChart
                key={color}
                visible={croppedSeries != null}
                axis="y"
                style={{ close: { normal: { stroke: color, "strokeWidth": 2 } } }}
                columns={["close"]}
                series={croppedSeries}
            />
        );
    };

    barChart = (series: any, timeRange: TimeRanges, color: string) => {
        if (series === undefined) {
            return <BarChart visible={false} axis="null" series={new TimeSeries()} />
        }

        const croppedSeries = series.crop(timeRange);
        return (
            <BarChart
                axis="y"
                style={{ volume: { normal: { opacity: 1, fill: color, stroke: color } } }}
                columns={["volume"]}
                series={croppedSeries}
            />
        );
    };

    renderFlagMarker = (chart: ChartModel) => {
        if (!chart.tracker) {
            return <EventMarker visible={false} />;
        }

        let info: any = [{ label: "Price", value: chart.trackerPriceValue, color: "#FF6230" }, { label: "Symbol", value: chart.symbol }];

        if (this.state.showVolumeChart) {
            info.push({ label: "RSI", value: chart.trackerRSIValue });
        }
        return (
            <EventMarker
                type="flag"
                axis="y"
                event={chart.trackerEvent}
                column="close"
                info={info}
                infoTimeFormat="%Y-%m-%d"
                infoWidth={120}
                infoHeight={50}
                markerRadius={4}
                markerStyle={{ fill: "#FF6230" }}
                infoStyle={{
                    label: {
                        fontWeight: 500,
                        fontSize: 14,
                        font: '"sans-serif"',
                        fill: 'black',
                    }
                }}
            />
        );
    };

    handleTrackerChanged = (t: Date) => {
        if (t) {
            const array = [...this.state.Charts];
            array.forEach((chart: ChartModel) => {
                const priceTicks = chart.candleSeries.atTime(t);
                const eventTime = new Date(
                    priceTicks.begin().getTime() + (priceTicks.end().getTime() - priceTicks.begin().getTime()) / 2
                );
                const eventValue = priceTicks.get("close").toString();
                chart.tracker = eventTime;
                chart.trackerEvent = priceTicks;
                chart.trackerPriceValue = eventValue;
                if (chart.rsiChart) {
                    chart.trackerRSIValue = chart.rsiChart.atTime(t).get("close").toString();
                }

            });
            this.setState({ Charts: array });
        }
        else {
            const array = [...this.state.Charts];
            array.forEach((chart: ChartModel) => {
                chart.tracker = null;
                chart.trackerEvent = null;
                chart.trackerPriceValue = null;
                chart.trackerRSIValue = null;
            });
            this.setState({ Charts: array });
        }
    };

    renderRSICharts() {
        if (!this.state.showRsiChart) {
            return <div />;
        }

        const baselineStyle = {
            line: {
                stroke: "steelblue",
                strokeWidth: 1,
                opacity: 0.4,
                strokeDasharray: "none"
            },
            label: {
                fill: "steelblue"
            }
        };

        return <Resizable>
            <ChartContainer
                timeRange={this.state.timeRange}
                hideWeekends={true}
                enablePanZoom={true}
                onTimeRangeChanged={this.handleTimeRangeChange}
                timeAxisStyle={{ axis: { fill: "none", stroke: "none" } }}
                onTrackerChanged={(date: Date) => this.handleTrackerChanged(date)}
            >
                {this.state.Charts.map(((chart: ChartModel) => {
                    const croppedRsi = chart.rsiChart.crop(this.state.timeRange);
                    return <ChartRow height="200" axisMargin={0} key={chart.symbol + "-RSI-chartRow"}>
                        <Charts>
                            {this.lineChart(croppedRsi, this.state.timeRange, Black)}
                            <Baseline axis="y" style={baselineStyle} value={70} label="70" position="right" />
                            <Baseline axis="y" style={baselineStyle} value={30} label="30" position="right" />
                        </Charts>
                        <YAxis
                            id="y"
                            label={"RSI: " + chart.symbol}
                            min={0}
                            max={100}
                            width="60"
                        />
                    </ChartRow>;
                }))}

            </ChartContainer>
        </Resizable>
    }

    renderVolumeCharts() {
        if (!this.state.showVolumeChart) {
            return <svg><g /></svg>;
        }

        return <Resizable>
            <ChartContainer
                timeRange={this.state.timeRange}
                hideWeekends={true}
                enablePanZoom={true}
                onTimeRangeChanged={this.handleTimeRangeChange}
                timeAxisStyle={{ axis: { fill: "none", stroke: "none" } }}
                onTrackerChanged={(date: Date) => this.handleTrackerChanged(date)}
            >
                {this.state.Charts.map(((chart: ChartModel) => {
                    const croppedVolumeSeriesGreen = chart.volumeSeriesGreen.crop(this.state.timeRange);
                    const croppedVolumeSeriesRed = chart.volumeSeriesRed.crop(this.state.timeRange);

                    return <ChartRow height="150" axisMargin={0} key={chart.symbol + "-volume-chartRow"}>
                        <Charts>
                            {this.barChart(croppedVolumeSeriesGreen, this.state.timeRange, Green)}
                            {this.barChart(croppedVolumeSeriesRed, this.state.timeRange, Red)}
                        </Charts>
                        <YAxis
                            id="y"
                            label={"Volume: " + chart.symbol}
                            min={Math.min(croppedVolumeSeriesGreen.min("volume"), croppedVolumeSeriesRed.min("volume"))}
                            max={Math.max(croppedVolumeSeriesGreen.max("volume"), croppedVolumeSeriesRed.max("volume"))}
                            width="60"
                        />
                    </ChartRow>;
                }))}

            </ChartContainer>
        </Resizable>
    }


    renderCharts() {
        const colors: string[] = [Green, Red, Purple, DarkGrey];
        return (
            <Resizable>
                <ChartContainer
                    timeRange={this.state.timeRange}
                    hideWeekends={true}
                    enablePanZoom={true}
                    onTimeRangeChanged={this.handleTimeRangeChange}
                    timeAxisStyle={{ axis: { fill: "none", stroke: "none" } }}
                    onTrackerChanged={(date: Date) => this.handleTrackerChanged(date)}
                >
                    {this.state.EquityChart ?
                        <ChartRow>
                            <Charts>
                                {this.lineChart(this.state.EquityChart, this.state.timeRange, Black)}
                            </Charts>
                            <YAxis
                                id="y"
                                label={"Balance"}
                                min={this.state.EquityChart.min("close")}
                                max={this.state.EquityChart.max("close")}
                                format=",.0f"
                                width="60"
                                type={this.state.mode}
                            />
                        </ChartRow> :
                        []
                    }
                    {this.state.Charts.map(((chart: ChartModel) => {
                        const croppedSeries = chart.candleSeries.crop(this.state.timeRange);
                        return (<ChartRow height="300" key={chart.symbol + "-candles-chartRow"}>
                            <Charts>
                                {this.lineChart(chart.candleSeries, this.state.timeRange, Black)}
                                {this.scatterChart(chart.buyEvents, this.state.timeRange, Green)}
                                {this.scatterChart(chart.sellEvents, this.state.timeRange, Red)}
                                {chart.signalCharts.map((chart: any, i: number) => {
                                    return this.lineChart(chart, this.state.timeRange, colors[i])
                                })}
                                {this.renderFlagMarker(chart)}
                            </Charts>
                            <YAxis
                                id="y"
                                label={"Price " + chart.symbol}
                                min={croppedSeries.min("close")}
                                max={croppedSeries.max("close")}
                                format=",.0f"
                                width="60"
                                type={this.state.mode}
                            />
                        </ChartRow>);
                    }))}
                </ChartContainer>
            </Resizable>
        );
    };


    // Unfortunately react time series can not manage breaking out LineCharts/BarCharts into 
    // separate components without breaking.
    render() {
        if (this.state === null) {
            return <span>Loading {this.props.Symbols} graph</span>;
        }

        return <div className="graph-container">
            <div className={this.state.mode + "-mode"}>
                <div className="log-control">
                    <span className="linear" onClick={this.setModeLinear}>Linear</span>
                    <span> | </span>
                    <span className="log" onClick={this.setModeLog}>Log</span>
                </div>
                <div className="volume" onClick={this.toggleVolumeChart}>
                    {this.state.showVolumeChart
                        ? <span>Disable</span>
                        : <span>Enable</span>
                    } volume chart
                </div>
                <StrategyPicker clickEvent={this.runStrategy} />
            </div>

            <hr />


            {this.renderCharts()}
            {this.renderRSICharts()}
            {this.renderVolumeCharts()}
            {this.renderStatisticsTable()}

        </div>
    }

    private renderStatisticsTable() {
        if (this.state.Statistics == undefined) {
            return null;
        }

        return (
            <table>
                <thead>
                    <tr>
                        <th>Metric</th>
                        <th>Result</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Maximum drawdown</th>
                        <th>{this.state.Statistics.maximumDrawdown}%</th>
                    </tr>
                    <tr>
                        <th>End balance</th>
                        <th>{this.state.Statistics.balance} SEK</th>
                    </tr>
                    <tr>
                        <th>Winrate</th>
                        <th>{this.state.Statistics.winRate}%</th>
                    </tr>
                    <tr>
                        <th>Percentage change</th>
                        <th>{this.state.Statistics.percentageChange}%</th>
                    </tr>
                    <tr>
                        <th>Price change</th>
                        <th>{this.state.Statistics.priceChange} SEK</th>
                    </tr>
                    <tr>
                        <th>Total trades</th>
                        <th>{this.state.Statistics.totalTrades}</th>
                    </tr>
                    <tr>
                        <th>Transaction fee cost</th>
                        <th>{this.state.Statistics.totalTransactionFeeCost} SEK</th>
                    </tr>
                    <tr>
                        <th>Buy and hold</th>
                        <th>{this.state.Statistics.buyAndHoldChange}%</th>
                    </tr>
                </tbody>
            </table>
        );
    }
}

export { Graph };