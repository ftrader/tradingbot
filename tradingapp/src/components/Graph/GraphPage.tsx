﻿import React from "react";
import { withRouter } from 'react-router';
import axios from "axios";
import './Graph.css';
import { Graph } from "./Graph";
import { BASE_API_URL } from "../../base";

interface ISymbolsState {
    Symbols: Symbol[];
    AddedGraphs: string[];
    Input: string;
    SelectedSymbols: string[];
    From: string;
    To: string;
    CurrentFrom: string;
    CurrentTo: string;
}

interface Symbol {
    name: string;
    symbol: string;
    lastFetched: string;
    active: boolean | false; // Not from API.
}

class GraphPage extends React.Component<{}, ISymbolsState> {
    // Default params
    defaultFrom = "2010";
    defaultTo = "2022";

    constructor(props: {}) {
        super(props);

        this.addSymbols = this.addSymbols.bind(this);
        this.handleFromChange = this.handleFromChange.bind(this);
        this.handleToChange = this.handleToChange.bind(this);
    }


    componentDidMount(): void {
        axios.get(BASE_API_URL + "/symbol")
            .then((response) => {
                
                response.data.forEach((symbol: Symbol) => {
                    symbol.active = true;
                });
                
                this.setState({
                    Symbols: response.data,
                    AddedGraphs: [],
                    Input: response.data[0].symbol,
                    SelectedSymbols: response.data,
                    From: this.defaultFrom,
                    CurrentFrom: this.defaultFrom,
                    To: this.defaultTo,
                    CurrentTo: this.defaultTo
                });
            });
    }

    addSymbols() {
        const symbols = this.state.Symbols.filter(x => x.active).map(x => x.symbol);
        this.setState({
            AddedGraphs: symbols
        });
    }

    // addAllSymbols() {
    //     this.setState({
    //         AddedGraphs: this.state.Symbols.map(x => x.symbol)
    //     });
    // }

    onToggle(stock: Symbol) {
        console.log(stock);
        const symbols = [...this.state.Symbols];
        const index = symbols.indexOf(stock);
        symbols[index].active = !symbols[index].active;
        this.setState({
            Symbols: symbols
        });
    }

    renderSymbolSelector() {
        if (this.state == null) {
            return <div>API is probably down.</div>;
        }

        return <div className="flex-row align-items">
            <h2>Add graph </h2>
            <div id="symbols-selector">
                {this.state.Symbols.map((stock: Symbol) => {
                    let className = "symbol-item";
                    className += stock.active ? " active" : " inactive";
                    return <div className={className} onClick={() => this.onToggle(stock)} key={stock.symbol}>{stock.name}</div>
                })}
            </div>
            <div className="button" onClick={this.addSymbols}><span>Add</span></div>
        </div>
    }

    renderGraphComponents() {
        if (this.state == null || this.state.AddedGraphs.length === 0) {
            return <h1>No graphs added.</h1>
        }
        return <Graph Symbols={this.state.AddedGraphs} From={this.state.CurrentFrom + "-01-01"} To={this.state.CurrentTo + "-12-31"} />;
    }

    handleFromChange(event: React.ChangeEvent<HTMLInputElement>) {
        if (event.target.value.length === 4) {
            this.setState({
                From: event.target.value,
                CurrentFrom: event.target.value
            });
        } else {
            console.log(event.target.value);
            this.setState({ From: event.target.value });
        }
    }

    handleToChange(event: React.ChangeEvent<HTMLInputElement>) {
        if (event.target.value.length === 4) {
            this.setState({
                To: event.target.value,
                CurrentTo: event.target.value
            });
        } else {
            this.setState({ To: event.target.value });
        }
    }

    renderTimeSelectors() {
        if (this.state == null) {
            return <span>Hmm</span>;
        }
        return <div className="time-labels">
            <label className="from">
                <span>From:</span>
                <input type="text" name="from" onChange={this.handleFromChange} value={this.state.From} />
            </label>
            <label className="to">
                <span>To:</span>
                <input type="text" name="to" onChange={this.handleToChange} value={this.state.To} />
            </label>
        </div>;
    }

    render() {
        return <div className="symbol">
            {this.renderSymbolSelector()}
            {this.renderTimeSelectors()}
            {this.renderGraphComponents()}
        </div>
    }
}

export { GraphPage, withRouter };