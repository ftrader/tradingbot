﻿import React from "react";
import './Symbols.css';
import axios from "axios";
import moment, { Moment } from "moment";
import { BASE_API_URL } from "../../base";
import { AddSymbol } from "./AddSymbol";

interface ISymbolsState {
    Symbols: Symbol[];
}

interface Symbol {
    name: string;
    symbol: string;
    lastFetched: Date;
}

class Symbols extends React.Component<{}, ISymbolsState> {

    constructor(props: {}) {
        super(props);
        this.updateStocks = this.updateStocks.bind(this);
        this.updateSymbolsList = this.updateSymbolsList.bind(this);
    }

    componentDidMount(): void {
        this.updateSymbolsList();
    }

    updateSymbolsList() {
        console.log("updating list");
        axios.get(BASE_API_URL + "/symbol")
            .then((response) => {
                this.setState({
                    Symbols: response.data
                });
            });
    }


    updateStocks() {
        axios.post(BASE_API_URL + "/symbol/update")
            .then((response) => {
                console.log(response);
                this.updateSymbolsList();
            });
    }

    renderTableHead() {
        return (
            <thead>
                <tr>
                    <th>Stock</th>
                    <th>Symbol</th>
                    <th>Last fetched</th>
                    <th onClick={this.updateStocks} style={{ cursor: "pointer" }}>Update all stocks</th>
                </tr>
            </thead>
        )
    }

    renderTableBody() {
        if (this.state == null) {
            return <div />;
        }

        return this.state.Symbols.map((stock: Symbol, index: number) => {
            const receivedDataFromYesterday = this.yesterdaysDate(stock.lastFetched);
            return (
                <tbody key={index}>
                    <tr>
                        <td>{stock.name}</td>
                        <td>{stock.symbol}</td>
                        <td>{this.formatDate(moment(stock.lastFetched))}</td>
                        {receivedDataFromYesterday ?
                            <td>-</td>
                            :
                            <td onClick={this.updateStocks} style={{ cursor: "pointer" }}>Update</td>
                        }
                    </tr>
                </tbody>
            )
        })
    }

    yesterdaysDate(date: Date) {
        return this.formatDate(moment(date)) === this.formatDate(moment().subtract(1, "days"));
    }

    formatDate(date: Moment) {
        return date.format("YYYY-MM-DD");
    }
    render() {
        return <div className="symbol">
            <AddSymbol updateSymbolsList={this.updateSymbolsList} />

            <div className="inline-block">
                <h2 className="downloaded-stocks">Downloaded stocks</h2>
                <table className="">
                    {this.renderTableHead()}
                    {this.renderTableBody()}
                </table>
            </div>

        </div>
    }
}

export { Symbols };
