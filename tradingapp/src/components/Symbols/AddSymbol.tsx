﻿import React from "react";
import './Symbols.css';
import axios from "axios";
import { BASE_API_URL } from "../../base";

interface IProps {
    updateSymbolsList: () => void;
}

interface ISymbolState {
    symbol: string;
}

class AddSymbol extends React.Component<IProps, ISymbolState> {

    constructor(props: IProps) {
        super(props);
        this.handleSymbolChange = this.handleSymbolChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount(): void {
        this.setState({ symbol: '' })
    }

    handleSymbolChange(event: React.ChangeEvent<HTMLInputElement>) {
        this.setState({ symbol: event.target.value })
    }

    handleSubmit(event: React.FormEvent) {
        event.preventDefault();
        console.log('Symbol: ' + this.state.symbol);

        if (this.state.symbol === '') {
            alert("Fill in more data.");
            return;
        }

        axios.post(BASE_API_URL + "/symbol/" + this.state.symbol)
            .then((response) => {
                console.log(response);
                this.props.updateSymbolsList();
            });

    }

    render() {
        return (
            <div className="inline-block">
                <h2 className="downloaded-stocks">Track stock</h2>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Symbol:
                        <input type="text" name="symbol" onChange={this.handleSymbolChange} />
                    </label>
                    <input type="submit" value="Submit" />
                </form>
            </div>
        )
    }
}

export { AddSymbol };
