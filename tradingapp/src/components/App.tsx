﻿import React from 'react';
import './App.css';
import { Home } from "./Home/Home";
import { GraphPage } from "./Graph/GraphPage";
import { Symbols } from "./Symbols/Symbols";
import { Navbar } from "./Navbar/Navbar";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import { BacktestPage } from "./Backtest/BacktestPage";

function App() {
	return (
		<BrowserRouter>
			<Navbar />
			<div className="content">
				<link href="/path/to/c3.css" rel="stylesheet" />
				<script src="/path/to/d3.v5.min.js" />
				<script src="/path/to/c3.min.js" />

				<Switch>
					<Route exact path="/" component={Home} />
					<Route exact path="/graph" component={GraphPage} />
					<Route exact path="/backtest" component={BacktestPage} />
					<Route exact path="/symbols" component={Symbols} />
				</Switch>
			</div>
		</BrowserRouter>
	);
}

export default App;
