﻿import React from "react";
import { Link } from "react-router-dom";

interface NavbarItemProps {
    value: string;
    link: string;
}

class NavbarItem extends React.Component<NavbarItemProps, {}> {
    render() {
        return <li><Link to={this.props.link}>{this.props.value}</Link></li>
    }
}

export { NavbarItem };