﻿import React from "react";
import './Navbar.css';
import { NavbarItem } from "./NavbarItem";

class Navbar extends React.Component {

    render() {
        return <ul className="navbar">
            <NavbarItem value="Home" link="/" />
            <NavbarItem value="Graphs" link="/graph" />
            <NavbarItem value="Backtest" link="/backtest" />
            <NavbarItem value="Symbols" link="/symbols" />
        </ul>;
    }
}

export { Navbar };