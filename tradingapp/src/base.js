﻿export const BASE_API_URL = process.env.NODE_ENV === 'production'
    ? process.env.REACT_APP_BASE_URL_PROD
    : process.env.REACT_APP_BASE_URL_LOCAL;

export const Green = "#006914";
export const Red = "#A61C1C";
export const Black = "#000000";
export const Grey = "#666666";
export const DarkGrey = "#333333";
export const Purple = "#31082d";